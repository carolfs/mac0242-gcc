package gui;

import javax.swing.JPanel;

/**
 * Interface para uma caravana.
 * @see units.Caravan
 */
public class CaravanView extends UnitView {
    private units.Caravan unit;
    static private java.awt.Image image;
    
    /**
     * Cria uma visualização para uma caravana.
     * @param unit a caravana
     */
    public CaravanView(units.Caravan unit)
    {
        this.unit = unit;
        if (image == null)
            image = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("caravan.png"));
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
    }
    
    @Override
    protected units.Unit getUnit() {
        return unit;
    }
    
    @Override
    public java.awt.Image getImage()
    {
        return image;
    }
    
    @Override
    protected String getUnitName()
    {
        return "Caravana";
    }
}
