package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;

/**
 * Interface para construtores
 * @see units.Builder
 */
public class BuilderView extends UnitView {
    private units.Builder unit;
    static private java.awt.Image image, isBuilding, isUnbuilding;
    private String statusBuilding;
    
    /**
     * Cria uma visualização para um construtor.
     * @param unit o construtor
     */
    public BuilderView(units.Builder unit)
    {
        this.unit = unit;
        if (image == null) {
            image = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("builder.png"));
            isBuilding = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("isBuilding.png"));
            isUnbuilding = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("isUnbuilding.png"));
        }
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
        JButton button;
        if (unit.isBusy()) {
            button = new JButton("Cancelar");
            button.addActionListener(new CancelButtonListener());
            panel.add(button);
        }
        else {
            super.addButtons(panel);
            
            // Construções
            
            if (unit.getPosition().allowsBuild(global.Global.BUILD_WALL) && unit.canBuild(global.Global.BUILD_WALL)) {
                button = new JButton("Construir Muro");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_WALL, "muro"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_DIRT_ROAD) && unit.canBuild(global.Global.BUILD_DIRT_ROAD)) {
                button = new JButton("Construir Estrada de Terra");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_DIRT_ROAD, "estrada de terra"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_BASE) && unit.canBuild(global.Global.BUILD_BASE)) {
                button = new JButton("Construir Base Militar");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_BASE, "base militar"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_HOUSE) && unit.canBuild(global.Global.BUILD_HOUSE)) {
                button = new JButton("Construir Casa");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_HOUSE, "casa"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_TOWER) && unit.canBuild(global.Global.BUILD_TOWER)) {
                button = new JButton("Construir Torre");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_TOWER, "torre"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_WAREHOUSE) && unit.canBuild(global.Global.BUILD_WAREHOUSE)) {
                button = new JButton("Construir Armazém");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_WAREHOUSE, "armazém"));
                panel.add(button);
            }
            
            if (unit.getPosition().getBuilding() != null) {
                
                // Reparo
                
                if (unit.getPosition().getBuilding().getStatus() == global.Global.AWAITING_REPAIR) {
                    button = new JButton("Consertar Construção");
                    button.addActionListener(new RepairButtonListener());
                    panel.add(button);
                }
                
                // Demolições
                
                button = new JButton("Demolir Construção");
                button.addActionListener(new UnbuildButtonListener(global.Global.BUILD_BUILDING, "construção"));
                panel.add(button);
            }
            
            // Demolições
            
            if (unit.getPosition().hasDirtRoad()) {
                button = new JButton("Demolir Estrada de Terra");
                button.addActionListener(new UnbuildButtonListener(global.Global.BUILD_DIRT_ROAD, "estrada de terra"));
                panel.add(button);
            }
            if (unit.getPosition().hasWall()) {
                button = new JButton("Demolir Muro");
                button.addActionListener(new UnbuildButtonListener(global.Global.BUILD_WALL, "muro"));
                panel.add(button);
            }
            if (unit.getPosition().hasForest()) {
                button = new JButton("Derrubar Floresta");
                button.addActionListener(new UnbuildButtonListener(global.Global.UNBUILD_FOREST, "floresta"));
                panel.add(button);
            }
        }
    }
    
    @Override
    public void drawUnit(Graphics2D g2d, AffineTransform trans, CoordinateConverter converter) {
        super.drawUnit(g2d, trans, converter);
        if (unit.getIsBuilding() || unit.getIsRepairing()) {
            g2d.drawImage(isBuilding, trans, getMapView());
        }
        else if (unit.getIsUnbuilding()) {
            g2d.drawImage(isUnbuilding, trans, getMapView());
        }
    }
    
    @Override
    protected units.Unit getUnit() {
        return unit;
    }
    
    @Override
    protected String getStatus() {
        if (unit.getIsBuilding()) {
            String status = "Construindo " + statusBuilding + " ";
            status += "(" + unit.getTurnsLeft() + ")";
            return status;
        }
        else if (unit.getIsUnbuilding()) {
            String status = "Demolindo " + statusBuilding + " ";
            status += "(" + unit.getTurnsLeft() + ")";
            return status;
        }
        return null;
    }
    
    @Override
    public Image getImage()
    {
        return image;
    }
    
    @Override
    protected String getUnitName()
    {
        return "Construtor";
    }
    
    private class BuildButtonListener implements ActionListener
    {
        final char build_option;
        final String building;
        
        BuildButtonListener(char build_option, String building) {
            this.build_option = build_option;
            this.building = building;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.build(build_option);
            statusBuilding = building;
            getMapView().updateUI();
        }
        
    }
    
    private class UnbuildButtonListener implements ActionListener
    {
        final char unbuild_option;
        final String building;
        
        UnbuildButtonListener(char unbuild_option, String building) {
            this.unbuild_option = unbuild_option;
            this.building = building;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.unbuild(unbuild_option);
            statusBuilding = building;
            getMapView().updateUI();
        }
        
    }
    
    private class CancelButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.cancel();
            getMapView().selectUnit(unit);
            getMapView().updateUI();
        }
        
    }
    
    private class RepairButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            unit.repair();
            getMapView().unselectUnit();
            getMapView().selectUnit(unit);
            getMapView().updateUI();
        }
        
    }
}
