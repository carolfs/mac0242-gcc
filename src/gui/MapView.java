package gui;

import global.Game;
import global.Global;
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import java.awt.event.*;
import mapping.*;
import buildings.*;
import com.sun.tools.javac.util.Pair;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.util.ArrayList;
import units.Unit;

/**
 * Visualização do mapa na tela.
 */
public class MapView extends JPanel implements MouseListener {
    private Image tile_sides, tile_unknown, tile_hl, tile_river, tile_plains, tile_forest, tile_movement,
            tile_attack, bridge_ver, bridge_hor, road, base, wall, dirt_road,
            fertile_land, farm, overrun, broken, well, selected_unit, tower, warehouse, house,
            tile_hills, tile_mountains;
    private BufferedImage tile_river_unseen, tile_plains_unseen, tile_forest_unseen,
            bridge_ver_unseen, bridge_hor_unseen, road_unseen, base_unseen,
            wall_unseen, dirt_road_unseen, fertile_land_unseen, farm_unseen,
            well_unseen, tower_unseen, warehouse_unseen, house_unseen,
            tile_hills_unseen, tile_mountains_unseen;
    private static final int TILE_SIDE = 50;
    private CoordinateConverter converter = new CoordinateConverter(TILE_SIDE);
    private Map map = Map.getInstance();
    private global.Game game = Game.getInstance();
    private units.Unit selectedUnit = null;
    private JViewport viewport = null;
    private ArrayList<Pair<Tile, Unit>> warnings = new ArrayList<Pair<Tile, Unit>>();
    
    /**
     * Índice da última unidade selecionada pelo sistema de navegação de unidades
     * aguardando ordens.
     */
    private int currentTurnUnitIndex = -1;
    
    /**
     * Cria uma visualização do mapa, carregando todas as imagens do jogo.
     */
    public MapView() {
        super();
        Toolkit tk = Toolkit.getDefaultToolkit();
        tile_sides = tk.getImage(this.getClass().getResource("tile_sides.png"));
        tile_movement = tk.getImage(this.getClass().getResource("tile_movement.png"));
        tile_unknown = tk.getImage(this.getClass().getResource("tile_unknown.png"));
        tile_attack = tk.getImage(this.getClass().getResource("tile_attack.png"));
        tile_plains = tk.getImage(this.getClass().getResource("tile_plains.png"));
        tile_river = tk.getImage(this.getClass().getResource("tile_river.png"));
        tile_forest = tk.getImage(this.getClass().getResource("tile_forest.png"));
        tile_hills = tk.getImage(this.getClass().getResource("tile_hills.png"));
        tile_mountains = tk.getImage(this.getClass().getResource("tile_mountains.png"));
        tile_hl = tile_movement;
        bridge_ver = tk.getImage(this.getClass().getResource("bridge_ver.png"));
        bridge_hor = tk.getImage(this.getClass().getResource("bridge_hor.png"));
        road = tk.getImage(this.getClass().getResource("road.png"));
        dirt_road = tk.getImage(this.getClass().getResource("dirt_road.png"));
        fertile_land = tk.getImage(this.getClass().getResource("fertile_land.png"));
        wall = tk.getImage(this.getClass().getResource("wall.png"));
        base = tk.getImage(this.getClass().getResource("military_base.png"));
        farm = tk.getImage(this.getClass().getResource("farm.png"));
        tower = tk.getImage(this.getClass().getResource("tower.png"));
        warehouse = tk.getImage(this.getClass().getResource("warehouse.png"));
        house = tk.getImage(this.getClass().getResource("house.png"));
        well = tk.getImage(this.getClass().getResource("well.png"));
        overrun = tk.getImage(this.getClass().getResource("overrun.png"));
        broken = tk.getImage(this.getClass().getResource("broken.png"));
        selected_unit = tk.getImage(this.getClass().getResource("selected_unit.png"));
        tile_river_unseen = makeUnseenImage(tile_river);
        tile_plains_unseen = makeUnseenImage(tile_plains);
        tile_forest_unseen = makeUnseenImage(tile_forest);
        tile_hills_unseen = makeUnseenImage(tile_hills);
        tile_mountains_unseen = makeUnseenImage(tile_mountains);
        bridge_ver_unseen = makeUnseenImage(bridge_ver);
        bridge_hor_unseen = makeUnseenImage(bridge_hor);
        road_unseen = makeUnseenImage(road);
        base_unseen = makeUnseenImage(base);
        wall_unseen = makeUnseenImage(wall);
        dirt_road_unseen = makeUnseenImage(dirt_road);
        fertile_land_unseen = makeUnseenImage(fertile_land);
        farm_unseen = makeUnseenImage(farm);
        well_unseen = makeUnseenImage(well);
        tower_unseen = makeUnseenImage(tower);
        warehouse_unseen = makeUnseenImage(warehouse);
        house_unseen = makeUnseenImage(house);
        addMouseListener(this);
        
        // Ajustar o tamanho do mapa
        setPreferredSize(new java.awt.Dimension(
                Math.round((map.getWidth() + 0.5f) * converter.getA()),
                Math.round(map.getHeight() * (converter.getS() + converter.getH()) + converter.getH())
                ));
    }
    
    private BufferedImage makeUnseenImage(Image image) {
        image = new ImageIcon(image).getImage();
        BufferedImage bi = new BufferedImage(Math.round(converter.getA()),
                Math.round(converter.getB()), BufferedImage.TYPE_INT_ARGB);
        Graphics g = bi.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        float scaleFactor = .6f;
        RescaleOp op = new RescaleOp(scaleFactor, 0, null);
        op.filter(bi, bi);
        return bi;
    }
    
    /**
     * Usar o destaque de movimento em lajotas destacadas.
     */
    public void highlightMovement() {
        tile_hl = tile_movement;
    }
    
    /**
     * Usar o destaque de ataque em lajotas destacadas.
     */
    public void highlightAttack() {
        tile_hl = tile_attack;
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        // Fundo branco
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth(), getHeight());
        paintTiles(g2d);
        paintTileSides(g2d);
        paintUnits(g2d);
    }

    private void paintTileSides(Graphics2D g2d) {
        // Desenha lajotas do mapa
        AffineTransform identity = new AffineTransform();
        AffineTransform trans = new AffineTransform();
        float x;
        float y = -converter.getS();
        int x_init = 0;
        int nx = 0;
        int ny = 0;
        while (ny <= map.getHeight()) {
            x = x_init * (converter.getR());
            x_init = ~x_init;
            nx = 0;
            while (nx <= map.getWidth()) {
                trans.setTransform(identity);
                trans.translate(Math.round(x), Math.round(y));
                g2d.drawImage(tile_sides, trans, this);
                x += converter.getA();
                nx++;
            }
            y += converter.getS() + converter.getH();
            ny++;
        }
    }
    
    private void paintTiles(Graphics2D g2d) {
        AffineTransform identity = new AffineTransform();
        AffineTransform trans = new AffineTransform();
        
        for (int i = 0; i < map.getWidth(); i++) {
            for (int j = 0; j < map.getHeight(); j++) {
                Tile tile = map.getGrid(i, j);
                converter.setArrayXY(i, j);
                trans.setTransform(identity);
                trans.translate(converter.getX(), converter.getY());
                
                if (tile.isUnknown()) {
                    g2d.drawImage(tile_unknown, trans, this);
                    continue;
                }
                
                Image terrain = null, structure = null, building = null;
                
                // Terrain
                
                if (tile.getTerrain() == Global.PLAINS)
                {
                    terrain = tile.isSeen() ? tile_plains : tile_plains_unseen;
                }
                else if (tile.getTerrain() == Global.RIVER)
                {
                    terrain = tile.isSeen() ? tile_river : tile_river_unseen;
                }
                else if (tile.getTerrain() == Global.HILL)
                {
                    terrain = tile.isSeen() ? tile_hills : tile_hills_unseen;
                }
                else if (tile.getTerrain() == Global.MOUNTAIN)
                {
                    terrain = tile.isSeen() ? tile_mountains : tile_mountains_unseen;
                }
                
                g2d.drawImage(terrain, trans, this);
                
                // Structures
                
                if (tile.hasRoad()) {
                    structure = tile.isSeen() ? road : road_unseen;
                    g2d.drawImage(structure, trans, this);
                }
                if (tile.hasDirtRoad()) {
                    structure = tile.isSeen() ? dirt_road : dirt_road_unseen;
                    g2d.drawImage(structure, trans, this);
                }
                if (tile.isFertile()) {
                    structure = tile.isSeen() ? fertile_land : fertile_land_unseen;
                    g2d.drawImage(structure, trans, this);
                }
                if (tile.hasBridge()) {
                    if (map.east(tile).getTerrain() == Global.RIVER && map.west(tile).getTerrain() == Global.RIVER) {
                        structure = tile.isSeen() ? bridge_ver : bridge_ver_unseen;
                    }
                    else {
                        structure = tile.isSeen() ? bridge_hor : bridge_hor_unseen;
                    }
                    g2d.drawImage(structure, trans, this);
                }
                if (tile.hasForest())
                {
                    structure = tile.isSeen() ? tile_forest : tile_forest_unseen;
                    g2d.drawImage(structure, trans, this);
                }
                if (tile.hasWall()) {
                    structure = tile.isSeen() ? wall : wall_unseen;
                    g2d.drawImage(structure, trans, this);
                }
                
                // Buildings
                
                if (tile.getBuilding() instanceof Base)
                {
                    building = tile.isSeen() ? base : base_unseen;
                }
                else if (tile.getBuilding() instanceof Farm)
                {
                    building = tile.isSeen() ? farm : farm_unseen;
                }
                else if (tile.getBuilding() instanceof House)
                {
                    building = tile.isSeen() ? house : house_unseen;
                }
                else if (tile.getBuilding() instanceof Well)
                {
                    building = tile.isSeen() ? well : well_unseen;
                }
                else if (tile.getBuilding() instanceof Tower)
                {
                    building = tile.isSeen() ? tower : tower_unseen;
                }
                else if (tile.getBuilding() instanceof Warehouse)
                {
                    building = tile.isSeen() ? warehouse : warehouse_unseen;
                }
                
                if (building != null) {
                    g2d.drawImage(building, trans, this);
                    if (tile.isSeen()) {
                        switch (tile.getBuilding().getStatus()) {
                            case Global.OVERRUN:
                                g2d.drawImage(overrun, trans, this);
                                break;
                            case Global.AWAITING_REPAIR:
                                g2d.drawImage(broken, trans, this);
                                break;
                        }
                    }
                }
                
                // Highlight
                
                if (tile.getHighlight() != Global.UNMARKED)
                {
                    g2d.drawImage(tile_hl, trans, this);
                    continue;
                }
                
                // Building status
                
                if (building != null) {
                    if (tile.isSeen()) {
                        switch (tile.getBuilding().getStatus()) {
                            case Global.OVERRUN:
                                g2d.drawImage(overrun, trans, this);
                                break;
                            default:
                                if (tile.getUnitCount() == 0) {
                                    g2d.setColor(Color.WHITE);
                                    g2d.setFont(new Font("Monospace", Font.PLAIN, 12));
                                    g2d.drawString(
                                        "" + tile.getBuilding().getResistance(),
                                        (float) (trans.getTranslateX() + converter.getA() * 0.5 - 10),
                                        (float) (trans.getTranslateY() + converter.getB() * 0.15)
                                    );
                                }
                        }
                    }
                }
            }
        }
    }
    
    private void paintUnits(Graphics2D g2d) {
        AffineTransform identity = new AffineTransform();
        AffineTransform trans = new AffineTransform();
        for (int i = 0; i < map.getWidth(); i++) {
            for (int j = 0; j < map.getHeight(); j++) {
                Tile tile = map.getGrid(i, j);
                if (tile.isSeen()) {
                    if (tile.getUnitCount() == 0)
                        continue;
                    else {
                        units.Unit unit = tile.getUnit(0);
                        converter.setPosition(tile);
                        trans.setTransform(identity);
                        trans.translate(converter.getX(), converter.getY());
                        unit.getView().drawUnit(g2d, trans, converter);
                        if (unit == selectedUnit)
                            g2d.drawImage(selected_unit, trans, this);
                        if (tile.getUnitCount() > 1) {
                            g2d.setColor(Color.WHITE);
                            g2d.setFont(new Font("Monospace", Font.PLAIN, 20));
                            g2d.drawString(
                                    "" + tile.getUnitCount(),
                                    (float) (trans.getTranslateX() + converter.getA() * 0.5 - 5),
                                    (float) (trans.getTranslateY() + converter.getB() * 0.9)
                            );
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Desseleciona a unidade selecionada. Supõe que haja uma unidade selecionada.
     */
    public void unselectUnit()
    {
        selectedUnit.getView().cancelDistanceAction();
        selectedUnit.getView().unload();
        selectedUnit = null;
    }
    
    /**
     * Seleciona uma unidade.
     * @param unit A unidade a ser selecionada.
     */
    public void selectUnit(Unit unit)
    {
        unit.getPosition().putOnTop(unit);
        selectedUnit = unit;
        unit.getView().load();
    }
    
    /**
     * Muda a unidade selecionada.
     * @param from a unidade que deve estar atualmente selecionada
     * @param to a unidade que se quer selecionar
     */
    public void switchSelectedUnit(Unit from, Unit to)
    {
        if (selectedUnit == from)
            selectUnit(to);
    }
    
    /**
     * Deve ser chamada antes do primeiro turno.
     */
    public void firstTurn()
    {
        // Iniciar a navegação do primeiro turno
        currentTurnUnitIndex = -1;
    }
    
    /**
     * Deve ser chamada no fim de um turno.
     */
    public void endTurn()
    {
        warnings.clear();
    }
    
     /**
     * Sinaliza a virada de turno para recarregar a visualização da unidade
     * selecionada, pois ela pode ter sido modificada (ou a unidade morreu).
     */
    public void nextTurn()
    {
        if (selectedUnit != null) {
            if (selectedUnit.isAlive())
                selectedUnit.getView().load();
            else
                unselectUnit();
        }
        currentTurnUnitIndex = -1;
        updateUI();
    }
    
    public void addWarning(Tile position, Unit unit) {
        Pair<Tile, Unit> pair = new Pair<Tile, Unit>(position, unit);
        warnings.add(pair);
    }
    
    public int warningCount() {
        return warnings.size();
    }
    
    @Override
    public void mouseClicked(MouseEvent me)
    {
        if (me.getButton() == MouseEvent.BUTTON1)
        {
            converter.setXY(me.getX(), me.getY());

            int x = converter.getArrayX();
            int y = converter.getArrayY();

            //System.out.println("(" + x + ", " + y + ")");
            
            if (x >= 0 && x < map.getWidth() && y >= 0 && y < map.getHeight())
            {
                mapping.Tile tile = map.getGrid(x, y);
                if (!tile.isSeen())
                    return;
                //System.out.println("Terrain = " + tile.getTerrain());
                //System.out.println("Distance = " + tile.getHighlight());
                
                if (selectedUnit != null) {
                    if (!selectedUnit.getView().act(tile)) {
                        selectedUnit.getView().unload();
                        // Se houver uma unidade, selecioná-la.
                        if (tile.getUnits().size() > 0) {
                            units.Unit unit = tile.getUnit(0);
                            selectedUnit = unit;
                            unit.getView().load();
                        }
                        else
                            selectedUnit = null;
                        updateUI();
                    }
                    else {
                        selectedUnit.getView().unload();
                        selectedUnit.getView().load();
                    }
                    updateUI();
                }
                // Se houver uma unidade, selecioná-la.
                else if (tile.getUnits().size() > 0) {
                    units.Unit unit = tile.getUnit(0);
                    selectedUnit = unit;
                    // Carregar painel de seleção
                    unit.getView().load();
                    updateUI();
                }
            }
        }

        if (me.getButton() == MouseEvent.BUTTON3)
        {
            converter.setXY(me.getX(), me.getY());

            int x = converter.getArrayX();
            int y = converter.getArrayY();

            if (x >= 0 && x < map.getWidth() && y >= 0 && y < map.getHeight())
            {
                mapping.Tile tile = map.getGrid(x, y);
                System.out.println("***************************");
                System.out.println("Tile: " + tile.getX() + "," + tile.getY());
                System.out.println("Terrain: " + tile.getTerrain());
                System.out.println("Movement cost: " + tile.getMovementCost());
                System.out.println("Noise: " + tile.getNoise());
                System.out.println("Visibility: " + tile.getVisibility());
                System.out.println("Alignment: " + tile.getAlignment());
                System.out.println("***************************");
                System.out.println();

            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent me) {}
    
    @Override
    public void mouseExited(MouseEvent me) {}
    
    @Override
    public void mousePressed(MouseEvent me) {}
    
    @Override
    public void mouseReleased(MouseEvent me) {}
    
    /**
     * Navegação das unidades pelo teclado.
     */
    
    private int getNextTurnUnitIndex(int previousIndex)
    {
        ArrayList<Unit> units = Game.getInstance().getUnits();
        for (int i = previousIndex + 1; i < units.size(); i++) {
            Unit unit = units.get(i);
            if (unit.isAwaitingCommand())
                return i;
        }
        return previousIndex;
    }
    
    private int getPreviousTurnUnitIndex(int nextIndex)
    {
        ArrayList<Unit> units = Game.getInstance().getUnits();
        for (int i = nextIndex - 1; i >= 0; i--) {
            Unit unit = units.get(i);
            if (unit.isAwaitingCommand())
                return i;
        }
        return nextIndex;
    }
    
    private void centersTile(Tile position)
    {
        converter.setPosition(position);
        int x = converter.getX() - viewport.getWidth() / 2;
        int y = converter.getY() - viewport.getHeight() / 2;
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        Point p = new Point(x, y);
        viewport.setViewPosition(p);
    }
    
    public void getNextTurnUnit()
    {
        ArrayList<Unit> units = Game.getInstance().getUnits();
        int nextTurnUnitIndex = getNextTurnUnitIndex(currentTurnUnitIndex);
        if (currentTurnUnitIndex != nextTurnUnitIndex) {
            currentTurnUnitIndex = nextTurnUnitIndex;
            Unit unit = units.get(currentTurnUnitIndex);
            if (unit != selectedUnit) {
                if (selectedUnit != null)
                    unselectUnit();
                selectUnit(unit);
                centersTile(unit.getPosition());
                updateUI();
            }
        }
    }
    
    public void getPreviousTurnUnit()
    {
        ArrayList<Unit> units = Game.getInstance().getUnits();
        int previousTurnUnitIndex = getPreviousTurnUnitIndex(currentTurnUnitIndex);
        if (currentTurnUnitIndex != previousTurnUnitIndex) {
            currentTurnUnitIndex = previousTurnUnitIndex;
            Unit unit = units.get(currentTurnUnitIndex);
            if (unit != selectedUnit) {
                if (selectedUnit != null)
                    unselectUnit();
                selectUnit(unit);
                centersTile(unit.getPosition());
                updateUI();
            }
        }
    }
    
    public boolean hasNextTurnUnit()
    {
        return currentTurnUnitIndex != getNextTurnUnitIndex(currentTurnUnitIndex);
    }
    
    public boolean hasPreviousTurnUnit()
    {
        return currentTurnUnitIndex != getPreviousTurnUnitIndex(currentTurnUnitIndex);
    }
    
    public void nextWarning() {
        Pair<Tile, Unit> pair = warnings.remove(0);
        Tile tile = pair.fst;
        Unit unit = pair.snd;
        centersTile(tile);
        if (unit != null) {
            if (unit.isAlive()) {
                if (unit.getPosition().isSeen()) {
                    if (selectedUnit == null) {
                        selectUnit(unit);
                        updateUI();
                    }
                    else if (selectedUnit.getPosition() != tile) {
                        unselectUnit();
                        selectUnit(unit);
                        updateUI();
                    }
                }
            }
            else if (unit.getAlignment() != Global.ZOMBIE) {
                JOptionPane.showMessageDialog(this,
                    "Uma unidade do tipo " + unit.getView().getUnitName() + " morreu aqui.",
                    "Aviso",
                    JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * @param viewport the viewport to set
     */
    public void setViewport(JViewport viewport) {
        this.viewport = viewport;
    }
}
