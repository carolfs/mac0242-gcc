package gui;

import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import units.Unit;

/**
 * Interface para um soldado
 * @see units.Soldier
 */
public class SoldierView extends UnitView {
    private units.Soldier soldier;
    static private java.awt.Image image, tower;
    
    /**
     * Cria uma visualização para um soldado.
     * @param soldier o soldado
     */
    public SoldierView(units.Soldier soldier)
    {
        this.soldier = soldier;
        if (image == null) {
            image = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("soldier.png"));
            tower = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("tower.png"));
        }
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
        if (!soldier.isInTower()) {
            super.addButtons(panel);
        }
        if (soldier.canDoRangedAttack()) {
            RangedAttackButtonAction action = new RangedAttackButtonAction();
            JButton rangedAttackButton = new JButton(action);
            rangedAttackButton.setText("Atirar (T)");
            rangedAttackButton.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_T, 0), "atirar");
            rangedAttackButton.getActionMap().put("atirar", action);
            panel.add(rangedAttackButton);
        }
        if (soldier.getPosition().getBuilding() instanceof buildings.Tower) {
            buildings.Tower tower = (buildings.Tower) soldier.getPosition().getBuilding();
            if (!tower.hasShooter() && tower.getStatus() == global.Global.READY_TO_USE) {
                JButton button = new JButton("Entrar na Torre");
                button.addActionListener(new EnterTowerButtonListener(tower));
                panel.add(button);
            }
            else if (soldier.isInTower()) {
                JButton button = new JButton("Sair na Torre");
                button.addActionListener(new LeaveTowerButtonListener());
                panel.add(button);
            }
        }
        buildings.Building building = soldier.getPosition().getBuilding();
        if (building != null && building.getStatus() == global.Global.OVERRUN && soldier.canClearBuilding()) {
            JButton button = new JButton("Desinfestar Construção");
            button.addActionListener(new ClearBuildingButtonListener());
            panel.add(button);
        }
        if (soldier.canMakeAmmo()) {
            JButton button = new JButton("Fazer munição");
            button.addActionListener(new MakeAmmoButtonListener());
            panel.add(button);
        }
    }
    
    @Override
    public java.awt.Image getImage()
    {
        if (soldier.isInTower())
            return tower;
        else
            return image;
    }
    
    protected String getHealthString()
    {
        if (soldier.isInTower())
            return soldier.getHealth() + " + " + soldier.getPosition().getBuilding().getResistance();
        else
            return "" + getUnit().getHealth();
    }
    
    @Override
    public void drawUnit(Graphics2D g2d, AffineTransform trans, CoordinateConverter converter) {
        if (!soldier.isInTower())
            super.drawUnit(g2d, trans, converter);
        else {
            g2d.setColor(Color.WHITE);
            g2d.setFont(new Font("Monospace", Font.PLAIN, 12));
            g2d.drawString(
                getHealthString(),
                (float) (trans.getTranslateX() + converter.getA() * 0.5 - 30),
                (float) (trans.getTranslateY() + converter.getB() * 0.15)
            );
        }
    }

    @Override
    protected Unit getUnit() {
        return soldier;
    }
    
    @Override
    protected String getUnitName()
    {
        if (soldier.isInTower())
            return "Soldado na Torre";
        else
            return "Soldado";
    }
    
    private class RangedAttackButtonAction extends AbstractAction
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            int range = soldier.getRange();
            buildings.Tower tower = soldier.getTower();
            if (tower != null) {
                range = tower.getRange();
            }
            
            cancelDistanceAction();
            if (mapping.Map.getInstance().hlRanged(soldier.getPosition(), range)) {
                getMapView().highlightAttack();
                setDistanceAction(new RangedAttackAction(range));
                getMapView().updateUI();
            }
        }
        
    }
    
    private class RangedAttackAction extends DistanceAction
    {
        private int range;
        
        RangedAttackAction(int range) {
            this.range = range;
        }

        @Override
        void cancel() {
            mapping.Map.getInstance().unHlRanged(soldier.getPosition(), range);
        }

        @Override
        void act(mapping.Tile tile) {
            if (tile.getUnitCount() > 0) {
                for (units.Unit unit: tile.getUnits()) {
                    if (unit.getAlignment() == global.Global.ZOMBIE) {
                        buildings.Tower tower = soldier.getTower();
                        if (tower != null) {
                            tower.rangedAttack(unit);
                        }
                        else {
                            soldier.rangedAttack(unit);
                        }
                        break;
                    }
                }
            }
            mapping.Map.getInstance().unHlRanged(soldier.getPosition(), range);
        }
        
    }
    
    private class EnterTowerButtonListener implements ActionListener
    {
        final private buildings.Tower tower;
        
        EnterTowerButtonListener(buildings.Tower tower) {
            this.tower = tower;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            tower.unitIn(soldier);
            getMapView().unselectUnit();
            getMapView().selectUnit(soldier);
            getMapView().updateUI();
        }
        
    }
    
    private class LeaveTowerButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            soldier.getTower().unitOut();
            getMapView().unselectUnit();
            getMapView().selectUnit(soldier);
            getMapView().updateUI();
        }
        
    }
    
    private class ClearBuildingButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            soldier.clearBuilding();
            getMapView().unselectUnit();
            getMapView().selectUnit(soldier);
            getMapView().updateUI();
        }
        
    }
    
    private class MakeAmmoButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            soldier.makeAmmo();
            getMapView().unselectUnit();
            getMapView().selectUnit(soldier);
            getMapView().updateUI();
        }
        
    }
}
