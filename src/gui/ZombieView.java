package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Interface para um zumbi
 * @see units.Zombie
 */
public class ZombieView extends UnitView {
    private units.Zombie unit;
    static private java.awt.Image image;
    
     /**
     * Cria uma visualização para um zumbi.
     * @param unit o zumbi
     */
    public ZombieView(units.Zombie unit)
    {
        this.unit = unit;
        if (image == null) {
            image = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("zombie.png"));
        }
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
        
    }
    
    @Override
    protected units.Unit getUnit() {
        return unit;
    }
    
    @Override
    public java.awt.Image getImage()
    {
        return image;
    }
    
    @Override
    protected String getUnitName()
    {
        return "Zumbi";
    }
}
