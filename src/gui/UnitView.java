package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import mapping.Tile;

/**
 * Superclasse para mostrar as propriedades de uma unidade quando ela é
 * selecionada.
 */
abstract public class UnitView {
    static private JPanel panel;
    static private MapView mapView;
    private DistanceAction distanceAction = null;
    static private ImageIcon warning;
    
    public UnitView() {
        if (warning == null)
            warning = new ImageIcon(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("warning-enabled.png")));
    }

    protected static MapView getMapView() {
        return mapView;
    }

    public static void setMapView(MapView aMapView) {
        mapView = aMapView;
    }
    
    protected String getStatus() {
        return null;
    }
    
    /**
     * Carrega a interface.
     */
    public void load()
    {
        JLabel label = new JLabel(getUnitName(), null, JLabel.CENTER);
        label.setIcon(new ImageIcon(getImage()));
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.anchor = GridBagConstraints.PAGE_START;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(label, c);
        JLabel healthLabel = new JLabel("Saúde: " + getHealthString(), null, JLabel.CENTER);
        c.gridy++;
        panel.add(healthLabel, c);
        String resources1 = "Mat: " + getUnit().getMaterials() + "/" + getUnit().getMaxMaterials() + " | " +
                "Mun: " + getUnit().getAmmo() + "/" + getUnit().getMaxAmmo();
        String resources2 =
                "C: " + getUnit().getFood() + "/" + getUnit().getMaxFood() + " | " +
                "A: " + getUnit().getWater() + "/" + getUnit().getMaxWater() + " ";
        JLabel resourcesLabel1 = new JLabel(resources1, null, JLabel.CENTER);
        c.gridy++;
        panel.add(resourcesLabel1, c);
        JLabel resourcesLabel2 = new JLabel(resources2, null, JLabel.CENTER);
        c.gridy++;
        panel.add(resourcesLabel2, c);
        String status = getStatus();
        if (status != null) {
            JLabel statusLabel = new JLabel(status, null, JLabel.CENTER);
            c.gridy++;
            panel.add(statusLabel, c);
        }
        if (getUnit().wasAttackedByZombies()) {
            JLabel warningLabel = new JLabel("Atacado por zumbis!", null, JLabel.CENTER);
            warningLabel.setIcon(warning);
            c.gridy++;
            panel.add(warningLabel, c);
        }
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(0, 1));
        addButtons(buttonsPanel);
        c.gridy++;
        c.weighty = 1;
        panel.add(buttonsPanel, c);
        c.gridy++;
        c.weighty = 0;
        if (getUnit().getPosition().getUnitCount() > 1) {
            RotateUnitAction action = new RotateUnitAction();
            JButton button = new JButton(action);
            button.setText(">> (R)");
            button.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_R, 0), "rotate");
            button.getActionMap().put("rotate", action);
            panel.add(button, c);
        }
        panel.validate();
    }
    
    protected String getHealthString()
    {
        return "" + getUnit().getHealth();
    }
    
    protected void addButtons(JPanel panel)
    {
        if (getUnit().canMove()) {
            MoveButtonAction action = new MoveButtonAction();
            JButton moveButton = new JButton(action);
            moveButton.setText("Mover (G)");
            moveButton.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_G, 0), "mover");
            moveButton.getActionMap().put("mover", action);
            panel.add(moveButton);
        }
        
        if (getUnit().canAttack()) {
            MeleeButtonAction action = new MeleeButtonAction();
            JButton meleeButton = new JButton(action);
            meleeButton.setText("Atacar (A)");
            meleeButton.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_A, 0), "atacar");
            meleeButton.getActionMap().put("atacar", action);
            panel.add(meleeButton);
        }
    }
    
    /**
     * Executa a acão da unidade.
     * @param tile lajota alvo da acão
     * @return se a ação foi ou não bem sucedida
     */
    public boolean act(mapping.Tile tile)
    {
        if (distanceAction != null) {
            distanceAction.act(tile);
            distanceAction = null;
            return true;
        }
        return false;
    }
    
    /**
     * Descarrega a interface.
     */
    public void unload()
    {
        panel.removeAll();
        panel.revalidate();
        panel.repaint();
    }
    
    public void addWarning()
    {
        getMapView().addWarning(getUnit().getPosition(), getUnit());
    }
    
    public void addWarning(mapping.Tile tile)
    {
        getMapView().addWarning(tile, getUnit());
    }
    
    /**
     * Desenha a unidade no mapa.
     * @param g2d
     * @param trans
     * @param converter 
     */
    public void drawUnit(Graphics2D g2d, AffineTransform trans, CoordinateConverter converter) {
        AffineTransform unitTrans = new AffineTransform(trans);
        unitTrans.translate(-5, -5);
        g2d.drawImage(getUnit().getView().getImage(), unitTrans, mapView);
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("Monospace", Font.PLAIN, 12));
        g2d.drawString(
                getHealthString(),
                (float) (trans.getTranslateX() + converter.getA() * 0.5 - 10),
                (float) (trans.getTranslateY() + converter.getB() * 0.1)
        );
    }

    abstract public Image getImage();
    abstract protected units.Unit getUnit();

    /**
     * @param panel the panel to set
     */
    static public void setPanel(JPanel panel) {
        UnitView.panel = panel;
    }
    
    abstract protected String getUnitName();

    /**
     * @param distanceAction the distanceAction to set
     */
    protected void setDistanceAction(DistanceAction distanceAction) {
        this.distanceAction = distanceAction;
    }
    
    public void cancelDistanceAction() {
        if (distanceAction != null)
            distanceAction.cancel();
    }
    
    abstract protected class DistanceAction
    {
        abstract void cancel();
        abstract void act(mapping.Tile tile);
    }
    
    private class MoveButtonAction extends AbstractAction
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            cancelDistanceAction();
            if (mapping.Map.getInstance().hlMoveable(getUnit().getPosition(), getUnit().getSteps())) {
                getMapView().highlightMovement();
                setDistanceAction(new MoveAction());
                getMapView().updateUI();
            }
        }
        
    }
    
    private class MoveAction extends DistanceAction
    {

        @Override
        void cancel() {
            mapping.Map.getInstance().unHlMoveable(getUnit().getPosition(),
                    getUnit().getSteps());
        }

        @Override
        void act(Tile tile) {
            mapping.Tile pos = getUnit().getPosition();
            int steps = getUnit().getSteps();

            if (tile.getHighlight() != global.Global.UNMARKED) {
                mapping.Map.getInstance().move(getUnit(), tile);
            }

            mapping.Map.getInstance().unHlMoveable(pos, steps);
        }
        
    }
    
    private class MeleeButtonAction extends AbstractAction
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            cancelDistanceAction();
            if (mapping.Map.getInstance().hlMelee(getUnit().getPosition())) {
                getMapView().highlightAttack();
                setDistanceAction(new MeleeAction());
                getMapView().updateUI();
            }
        }
        
    }
    
    private class RotateUnitAction extends AbstractAction
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            getUnit().getPosition().rotateUnits();
            getMapView().unselectUnit();
            getMapView().selectUnit(getUnit().getPosition().getUnit(0));
            getMapView().updateUI();
        }
        
    }
    
    private class MeleeAction extends DistanceAction
    {

        @Override
        void cancel() {
            mapping.Map.getInstance().unHlMelee(getUnit().getPosition());
        }

        @Override
        void act(Tile tile) {
            units.Unit zombie = null;
            for (units.Unit unit: tile.getUnits()) {
                if (unit.getAlignment() == global.Global.ZOMBIE) {
                    zombie = unit;
                    break;
                }
            }
            if (zombie != null) {
                getUnit().meleeAttack(zombie);
            }
            mapping.Map.getInstance().unHlMelee(getUnit().getPosition());
        }
        
    }
}
