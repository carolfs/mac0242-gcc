package gui;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import javax.swing.JButton;
import javax.swing.JPanel;


/**
 * Interface para um fazendeiro.
 * @see units.Farmer
 */
public class FarmerView extends UnitView {
    private units.Farmer unit;
    static private java.awt.Image image, isBuilding, isFarming;
    private String statusBuilding;
    
    /**
     * Cria uma visualização para um fazendeiro.
     * @param unit o fazendeiro
     */
    public FarmerView(units.Farmer unit)
    {
        this.unit = unit;
        if (image == null) {
            image = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("farmer.png"));
            isBuilding = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("isBuilding.png"));
            isFarming = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("isFarming.png"));
        }
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
        if (unit.getIsBuilding() || unit.isFarming()) {
            JButton button = new JButton("Cancelar");
            button.addActionListener(new CancelButtonListener());
            panel.add(button);
        }
        else {
            super.addButtons(panel);
            if (unit.getPosition().allowsBuild(global.Global.BUILD_FERTILE_LAND) && unit.canBuild(global.Global.BUILD_FERTILE_LAND)) {
                JButton button = new JButton("Construir Terra Fértil");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_FERTILE_LAND, "terra fértil"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_FARM) && unit.canBuild(global.Global.BUILD_FARM)) {
                JButton button = new JButton("Construir Fazenda");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_FARM, "fazenda"));
                panel.add(button);
            }
            if (unit.getPosition().allowsBuild(global.Global.BUILD_WELL) && unit.canBuild(global.Global.BUILD_WELL)) {
                JButton button = new JButton("Construir Poço");
                button.addActionListener(new BuildButtonListener(global.Global.BUILD_WELL, "poço"));
                panel.add(button);
            }
            if (unit.getPosition().getBuilding() instanceof buildings.Farm) {
                buildings.Farm farm = (buildings.Farm) unit.getPosition().getBuilding();
                if (farm.admitsNewFarmer()) {
                    JButton button = new JButton("Trabalhar na Fazenda");
                    button.addActionListener(new FarmButtonListener());
                    panel.add(button);
                }
            }
        }
    }
    
    @Override
    public void drawUnit(Graphics2D g2d, AffineTransform trans, CoordinateConverter converter) {
        super.drawUnit(g2d, trans, converter);
        if (unit.getIsBuilding()) {
            g2d.drawImage(isBuilding, trans, getMapView());
        }
        else if (unit.isFarming()) {
            g2d.drawImage(isFarming, trans, getMapView());
        }
    }
    
    @Override
    protected units.Unit getUnit() {
        return unit;
    }
    
    @Override
    protected String getStatus() {
        if (unit.getIsBuilding()) {
            String status = "Construindo " + statusBuilding + " ";
            status += "(" + unit.getTurnsLeft() + ")";
            return status;
        }
        else if (unit.isFarming()) {
            return "Trabalhando na fazenda";
        }
        return null;
    }
    
    private class BuildButtonListener implements ActionListener
    {
        final char build_option;
        final String building;
        
        BuildButtonListener(char build_option, String building) {
            this.build_option = build_option;
            this.building = building;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.build(build_option);
            statusBuilding = building;
            getMapView().updateUI();
        }
        
    }
    
    private class CancelButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.cancel();
            getMapView().selectUnit(unit);
            getMapView().updateUI();
        }
        
    }
    
    private class FarmButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.farm();
            getMapView().updateUI();
        }
        
    }
    
    @Override
    public java.awt.Image getImage()
    {
        return image;
    }
    
    @Override
    protected String getUnitName()
    {
        return "Fazendeiro";
    }
}
