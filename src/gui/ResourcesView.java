package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Visualização dos recursos (comida, água, munição, habitação, população e
 * materiais de construção).
 */
public class ResourcesView extends JPanel {
    private JLabel ammoStatus = new JLabel();
    private JLabel foodStatus = new JLabel();
    private JLabel housingStatus = new JLabel();
    private JLabel populationStatus = new JLabel();
    private JLabel materialsStatus = new JLabel();
    private JLabel waterStatus = new JLabel();
    private JLabel turnCount = new JLabel();
    
    /**
     * Arruma o painel inicialmente (tamanho, borda) e carrega os rótulos.
     */
    public void setUp() {
        setBorder(BorderFactory.createTitledBorder(null, "Recursos", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        Dimension maxSize = this.getMaximumSize();
        maxSize.setSize(maxSize.width, 130);
        setMaximumSize(maxSize);
        Dimension preferredSize = this.getPreferredSize();
        preferredSize.setSize(preferredSize.width, 130);
        setPreferredSize(preferredSize);

        add(ammoStatus);

        add(foodStatus);

        add(waterStatus);

        add(materialsStatus);

        add(housingStatus);

        add(populationStatus);
        
        add(turnCount);
        validate();
    }
    
    public void updateTurnCount(int turnCount)
    {
        this.turnCount.setText("Turno: " + (turnCount + 1));
        updateUI();
    }
    
    public void updateAmmo(int ammo, int maxAmmo)
    {
        ammoStatus.setText("Munição: " + ammo + " (" + maxAmmo + ")");
        updateUI();
    }
    
    public void updateFood(int food, int maxFood)
    {
        foodStatus.setText("Comida: " + food + " (" + maxFood + ")");
        updateUI();
    }
    
    public void updateWater(int water, int maxWater)
    {
        waterStatus.setText("Água: " + water + " (" + maxWater + ")");
        updateUI();
    }
    
    public void updateMaterials(int materials, int maxMaterials)
    {
        materialsStatus.setText("Materiais: " + materials + " (" + maxMaterials + ")");
        updateUI();
    }
    
    public void updateHousing(int housing)
    {
        housingStatus.setText("Habitações: " + housing);
        updateUI();
    }
    
    public void updatePopulation(int population)
    {
        populationStatus.setText("População: " + population);
        updateUI();
    }
}
