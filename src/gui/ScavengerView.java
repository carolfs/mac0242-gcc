package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Interface para um explorador
 * @see units.Scavenger
 */
public class ScavengerView extends UnitView {
    private units.Scavenger unit;
    static private java.awt.Image image, image_hidden;
    
    /**
     * Cria uma visualização para um explorador.
     * @param unit o explorador
     */
    public ScavengerView(units.Scavenger unit)
    {
        this.unit = unit;
        if (image == null) {
            image = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("scavenger.png"));
            image_hidden = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("scavenger_hidden.png"));
        }
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
        super.addButtons(panel);
        if (unit.canHide()) {
            JButton button = new JButton("Esconder-se");
            button.addActionListener(new HideButtonListener());
            panel.add(button);
        }
        if (unit.canPillage()) {
            JButton button = new JButton("Saquear");
            button.addActionListener(new PillageButtonListener());
            panel.add(button);
        }
    }
    
    @Override
    protected units.Unit getUnit() {
        return unit;
    }
    
    @Override
    public java.awt.Image getImage()
    {
        return unit.isHidden() ? image_hidden : image;
    }
    
    @Override
    protected String getStatus() {
        if (unit.isHidden()) {
            return "Escondido";
        }
        return null;
    }
    
    @Override
    protected String getUnitName()
    {
        return "Explorador";
    }
    
    private class HideButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            unit.hide();
            if (unit.isHidden()) {
                getMapView().unselectUnit();
                getMapView().updateUI();
            }
        }
        
    }
    
    private class PillageButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            unit.pillage();
            getMapView().updateUI();
        }
        
    }
}
