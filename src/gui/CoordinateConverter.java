package gui;

/**
 * Converte de coordenadas cartesianas para lajotas. Explicações todas no endereço
 * http://www.gamedev.net/page/resources/_/technical/game-programming/coordinates-in-hexagon-based-tile-maps-r1800
 */
class CoordinateConverter {
    // Parâmetros das lajotas
    private float s, h, r, a, b;
    private int arrayX, arrayY;
    private int x, y;
    
    public CoordinateConverter(float ss) {
        s = ss;
        h = (float) (.5 * s);
        r = (float) (.5 * Math.sqrt(3) * s);
        b = s + 2 * h;
        a = 2 * r;
    }

    public float getS() {
        return s;
    }

    public float getH() {
        return h;
    }

    public float getR() {
        return r;
    }

    /**
     * Recebe coordenadas em pixels da tela a serem convertidas para coordenadas
     * de lajotas.
     * @param x
     * @param y 
     */
    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
        int xsection = (int) (x / (a));
        float xsectpxl = x - xsection * a;
    
        int ysection = (int) (y / (h + s));
        float ysectpxl = y - ysection * (h + s);
        boolean sectTypA = (ysection % 2 == 0);
        float m = h / r;
        
        if (sectTypA) {
            if (ysectpxl < (h - xsectpxl * m)) {
                arrayY = (int) (ysection - 1);
                arrayX = (int) (xsection - 1);
            }
            else if (ysectpxl < (-h + xsectpxl * m)) {
                arrayY = (int) (ysection - 1);
                arrayX = (int) (xsection);
            }
            else {
                arrayY = (int) ysection;
                arrayX = (int) xsection;
            }
        }
        else {
            if (xsectpxl >= r) {
                if (ysectpxl < (2 * h - xsectpxl * m)) {
                    arrayY = (int) (ysection - 1);
                    arrayX = (int) (xsection);
                }
                else {
                    arrayY = (int) ysection;
                    arrayX = (int) xsection;
                }
            }
            else {
                if (ysectpxl < (xsectpxl * m)) {
                    arrayY = (int) (ysection - 1);
                    arrayX = (int) (xsection);
                }
                else {
                    arrayY = (int) ysection;
                    arrayX = (int) (xsection - 1);
                }
            }
        }
    }
    
    /**
     * Recebe coordenadas de lajotas a serem convertidas às coordenadas do pixel
     * superior esquerdo da lajota na tela.
     * @param arrayX
     * @param arrayY 
     */
    public void setArrayXY(int arrayX, int arrayY) {
        this.arrayX = arrayX;
        this.arrayY = arrayY;
        if (arrayY % 2 == 0) {
            x = Math.round(arrayX * a);
        }
        else {
            x = Math.round(arrayX * 2 * r + r);
        }
        y = Math.round(arrayY * (h + s));
    }
    
    /**
     * Recebe uma lajota e armazena as coordenadas dela no mapa e na tela.
     * @param tile 
     */
    public void setPosition(mapping.Tile tile) {
        setArrayXY(tile.getX(), tile.getY());
    }

    /**
     * Coordenada X da lajota no mapa.
     */
    public int getArrayX() {
        return arrayX;
    }

    /**
     * Coordenada Y da lajota no mapa.
     */
    public int getArrayY() {
        return arrayY;
    }

    public float getA() {
        return a;
    }

    public float getB() {
        return b;
    }

    /**
     * Coordenada X do pixel superior esquerdo da lajota no mapa.
     */
    public int getX() {
        return x;
    }

    /**
     * Coordenada Y do pixel superior esquerdo da lajota no mapa.
     */
    public int getY() {
        return y;
    }
}
