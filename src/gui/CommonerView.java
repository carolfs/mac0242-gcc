package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;

/**
 * Interface para um leigo
 * @see units.Commoner
 */
public class CommonerView extends UnitView {
    private units.Commoner unit;
    static private java.awt.Image image, training;
    String status = "";
    
    /**
     * Cria uma visualização para um leigo.
     * @param unit o leigo
     */
    public CommonerView(units.Commoner unit)
    {
        this.unit = unit;
        if (image == null) {
            image = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("commoner.png"));
            training = java.awt.Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("training.png"));
        }
    }
    
    @Override
    protected void addButtons(JPanel panel)
    {
        if (!unit.isTraining()) {
            super.addButtons(panel);
        }
        else {
            JButton button = new JButton("Cancelar");
            button.addActionListener(new CancelButtonListener());
            panel.add(button);
        }
        if (unit.getPosition().getBuilding() instanceof buildings.Base) {
            buildings.Base base = (buildings.Base) unit.getPosition().getBuilding();
            if (base.canTrain(unit)) {
                JButton button = new JButton("Treinar para Soldado");
                button.addActionListener(new TrainButtonListener(global.Global.SOLDIER, "soldado"));
                panel.add(button);

                button = new JButton("Treinar para Explorador");
                button.addActionListener(new TrainButtonListener(global.Global.SCAVENGER, "explorador"));
                panel.add(button);

                button = new JButton("Treinar para Fazendeiro");
                button.addActionListener(new TrainButtonListener(global.Global.FARMER, "fazendeiro"));
                panel.add(button);

                button = new JButton("Treinar para Construtor");
                button.addActionListener(new TrainButtonListener(global.Global.BUILDER, "construtor"));
                panel.add(button);
            }
        }
    }
    
    @Override
    public void drawUnit(Graphics2D g2d, AffineTransform trans, CoordinateConverter converter) {
        super.drawUnit(g2d, trans, converter);
        if (unit.isTraining()) {
            g2d.drawImage(training, trans, getMapView());
        }
    }
    
    @Override
    protected units.Unit getUnit() {
        return unit;
    }
    
    @Override
    protected String getStatus() {
        if (unit.isTraining()) {
            buildings.Base base = (buildings.Base) unit.getPosition().getBuilding();
            return status + " (" + base.getTurnsRemaining(unit) + ")";
        }
        else
            return null;
    }
    
    @Override
    public java.awt.Image getImage()
    {
        return image;
    }
    
    @Override
    protected String getUnitName()
    {
        return "Leigo";
    }
    
    public void finishedTraining(units.Unit unit)
    {
        getMapView().switchSelectedUnit(this.unit, unit);
        getMapView().updateUI();
    }
    
    private class TrainButtonListener implements ActionListener
    {
        final char professionOption;
        final String profession;
        
        public TrainButtonListener(char professionOption, String profession) {
            this.professionOption = professionOption;
            this.profession = profession;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            buildings.Base base = (buildings.Base) unit.getPosition().getBuilding();
            if (base.train(unit, professionOption)) {
                getMapView().unselectUnit();
                status = "Treinando para " + profession;
                getMapView().updateUI();
            }
        }
        
    }
    
    private class CancelButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ae) {
            getMapView().unselectUnit();
            unit.cancel();
            getMapView().selectUnit(unit);
            getMapView().updateUI();
        }
        
    }
}
