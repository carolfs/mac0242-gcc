package global;
import buildings.*;
import java.util.ArrayList;
import mapping.*;
import units.*;

/**
 * Inicializa e gerencia o jogo.
 */
public class Game
{
    private ArrayList<Unit> units = new ArrayList<Unit>();
    private ArrayList<Building> buildings = new ArrayList<Building>();
    
    static final private Game instance = new Game();

    private int food = 0;
    private int maxFood = 0;
    private int foodBalance = 0;
    
    private int water = 0;
    private int maxWater = 0;
    private int waterBalance = 0;

    private int ammo = 0;
    private int maxAmmo = 0;

    private int materials = 0;
    private int maxMaterials = 0;

    private int housing = 0;
    private int population = 0;

    private int zombies = 0;

    private gui.ResourcesView resourcesView;
    private gui.GameWindow gameWindow;
    
    static public Game getInstance() { return instance; }
    
    /**
     * Cria as unidades e construções iniciais no jogo.
     */
    public void loadInitialState()
    {
        Map map = Map.getInstance();
        Unit unit;

        unit = new Soldier(map.getGrid(9, 12));
        newUnit(unit);
        map.getGrid(9, 12).addUnit(unit);
        unit = new Soldier(map.getGrid(7, 14));
        newUnit(unit);
        map.getGrid(7,14).addUnit(unit);
        unit = new Soldier(map.getGrid(7, 15));
        newUnit(unit);
        map.getGrid(7,15).addUnit(unit);
        unit = new Soldier(map.getGrid(7, 11));
        newUnit(unit);
        map.getGrid(7, 11).addUnit(unit);

        unit = new Scavenger(map.getGrid(4, 8));
        newUnit(unit);
        map.getGrid(4, 8).addUnit(unit);

        unit = new Builder(map.getGrid(6, 9));
        newUnit(unit);
        map.getGrid(6, 9).addUnit(unit);

        unit = new Farmer(map.getGrid(8, 8));
        newUnit(unit);
        map.getGrid(8, 8).addUnit(unit);

        unit = new Commoner(map.getGrid(7, 7));
        newUnit(unit);
        map.getGrid(7, 7).addUnit(unit);
        unit = new Commoner(map.getGrid(6, 8));
        newUnit(unit);
        map.getGrid(6, 8).addUnit(unit);
        unit = new Commoner(map.getGrid(5, 8));
        newUnit(unit);
        map.getGrid(5, 8).addUnit(unit);

        Map.getInstance().scatterAI();

        addFood(maxFood);
        addWater(maxWater);
        addAmmo(maxAmmo);
        addMaterials(maxMaterials);

        resourcesView.updateAmmo(ammo, maxAmmo);
        resourcesView.updateFood(food, maxFood);
        resourcesView.updateWater(water, maxWater);
        resourcesView.updateMaterials(materials, maxMaterials);
        resourcesView.updateHousing(housing);
        resourcesView.updatePopulation(population);
        resourcesView.updateTurnCount(Map.getInstance().getTurn());
    }

    /**
     * @return the units
     */
    public ArrayList<units.Unit> getUnits()
    {
        return units;
    }

    /**
     * Adiciona uma nova unidade ao jogo.
     * @param unit a unidade a ser adicionada
     */
    public void newUnit(Unit unit)
    {
        units.add(unit);

        if (unit.getAlignment() == Global.ZOMBIE)
            zombies += 1;

        if (unit.getAlignment() == Global.HUMAN)
        {
            population += 1;
            waterBalance -= 1;
            foodBalance -= 1;
        }
        
        resourcesView.updateFood(food, maxFood);
        resourcesView.updateWater(water, maxWater);
        resourcesView.updatePopulation(population);
    }

    /**
     * Remove uma unidade do jogo.
     * @param unit a unidade a ser removida
     */
    public void removeUnit(Unit unit)
    {
        units.remove(unit);

        if (unit.getAlignment() != Global.HUMAN)
            return;

        population -= 1;
        waterBalance += 1;
        foodBalance += 1;

        if (resourcesView != null)
        {
            resourcesView.updateFood(food, maxFood);
            resourcesView.updateWater(water, maxWater);
            resourcesView.updatePopulation(population);
        }

        if (population == 0)
            youLose();
    }

    /**
     * Adiciona uma construção ao jogo.
     * @param building a construção a ser adicionada
     */
    public void newBuilding(Building building)
    {
        buildings.add(building);

        if (building.getStatus() != Global.READY_TO_USE)
            return;

        addBuilding(building);
    }

    public void addBuilding(Building building)
    {
        maxFood += building.getFoodCapacity();
        maxWater += building.getWaterCapacity();
        maxAmmo += building.getAmmoCapacity();
        maxMaterials += building.getWaterCapacity();
        housing += building.getHousingCapacity();

        if (resourcesView != null)
        {
            resourcesView.updateAmmo(ammo, maxAmmo);
            resourcesView.updateFood(food, maxFood);
            resourcesView.updateWater(water, maxWater);
            resourcesView.updateMaterials(materials, maxMaterials);
            resourcesView.updateHousing(housing);
        }
    }

    /**
     * Remove uma construção do jogo.
     * @param building a construção a ser removida
     */
    public void removeBuilding(Building building)
    {
        buildings.remove(building);

        if (building.getStatus() != Global.READY_TO_USE)
            return;

        loseBuilding(building);
    }

    public void loseBuilding(Building building)
    {
        maxFood -= building.getFoodCapacity();
        if (food > maxFood)
            food = maxFood;

        maxWater -= building.getWaterCapacity();
        if (water > maxWater)
            water = maxWater;

        maxAmmo -= building.getAmmoCapacity();
        if (ammo > maxAmmo)
            ammo = maxAmmo;

        maxMaterials -= building.getWaterCapacity();
        if (materials > maxMaterials)
            materials = maxMaterials;

        housing -= building.getHousingCapacity();

        if (resourcesView != null)
        {
            resourcesView.updateAmmo(ammo, maxAmmo);
            resourcesView.updateFood(food, maxFood);
            resourcesView.updateWater(water, maxWater);
            resourcesView.updateMaterials(materials, maxMaterials);
            resourcesView.updateHousing(housing);
        }
    }

    /**
     * Retorna a quantidade atual de munição.
     */
    public int getAmmo()
    {
        return ammo;
    }

    /**
     * Adiciona munição no jogo.
     * @param ammo quantidade de munição a ser adicionada
     * @return se a munição foi ou não adicionada
     */
    public boolean addAmmo(int ammo)
    {
        if (this.ammo + ammo <= 0)
            return false;

        this.ammo += ammo;

        if (this.ammo > maxAmmo)
            this.ammo = maxAmmo;

        resourcesView.updateAmmo(this.ammo, maxAmmo);
        return true;
    }

    /**
     * Retorna a quantidade máxima de munição que o jogador pode armazenar.
     */
    public int getMaxAmmo()
    {
        return maxAmmo;
    }

    /**
     * Retorna a quantidade de comida atual.
     */
    public int getFood()
    {
        return food;
    }

    /**
     * Adiciona comida ao jogo.
     * @param food quantidade de comida a ser adicionada
     * @return se a comida foi ou não adicionada
     */
    public boolean addFood(int food)
    {
        if (this.food + food <= 0)
            return false;

        this.food += food;

        if (this.food > maxFood)
            this.food = maxFood;

        resourcesView.updateFood(this.food, maxFood);
        return true;
    }

    /**
     * Retorna a quantidade máxima de comida que o jogador pode armazenar.
     */
    public int getMaxFood()
    {
        return maxFood;
    }

    public int getFoodBalance()
    {
        return foodBalance;
    }

    public int getMaterials()
    {
        return materials;
    }

    public boolean addMaterials(int materials)
    {
        if (this.materials + materials <= 0)
            return false;

        this.materials += materials;

        if (this.materials > maxMaterials)
            this.materials = maxMaterials;

        resourcesView.updateMaterials(this.materials, maxMaterials);
        return true;
    }

    public int getMaxMaterials()
    {
        return maxMaterials;
    }

    public int getWater()
    {
        return water;
    }

    public boolean addWater(int water)
    {
        if (this.water + water <= 0)
            return false;

        this.water += water;

        if (this.water > maxWater)
            this.water = maxWater;

        resourcesView.updateWater(this.water, maxWater);
        return true;
    }

    public int getMaxWater()
    {
        return maxWater;
    }

    public int getWaterBalance()
    {
        return waterBalance;
    }

    public int getHousing()
    {
        return housing;
    }

    public void addHousing(int housing)
    {
        this.housing += housing;
        resourcesView.updateHousing(this.housing);
    }

    public int getPopulation()
    {
        return population;
    }

    /**
     * Adiciona uma referência ao objeto que gerencia a visualização dos recursos ao jogador.
     * @param resourcesView o objeto que gerencia a visualização
     * @see gui.ResourcesView
     */
    public void setResourcesView(gui.ResourcesView resourcesView)
    {
        this.resourcesView = resourcesView;
        resourcesView.updateAmmo(ammo, maxAmmo);
        resourcesView.updateFood(food, maxFood);
        resourcesView.updateWater(water, maxWater);
        resourcesView.updateMaterials(materials, maxMaterials);
        resourcesView.updateHousing(housing);
        resourcesView.updatePopulation(population);
    }

    public int getZombies()
    {
        return zombies;
    }

    public void youWin() {
        gameWindow.endGameWin();
    }
    
    public void youLose() {
        gameWindow.endGameLoss();
    }

    /**
     * @param gameWindow the gameWindow to set
     */
    public void setGameWindow(gui.GameWindow gameWindow) {
        this.gameWindow = gameWindow;
    }
}
