package global;

/*
 * Definições utilizadas para clareza de código
 */

public class Global
{
    // tile terrain
    public static final char PLAINS = 'p';
    public static final char HILL = 'h';
    public static final char MOUNTAIN = 'm';
    public static final char RIVER = 'r';

    // tile structures
    public static final int BRIDGE = -1;
    public static final int FOREST = 1;
    public static final int ROAD = 2;
    public static final int DIRT_ROAD = 4;
    public static final int WALL = 8;
    public static final int FERTILE_LAND = 16;

    // tile movementCost
    public static final int INFINITY = 1024;

    // unit/building/tile alignment
    public static final char HUMAN = 'H';
    public static final char NEUTRAL = 'N';
    public static final char ZOMBIE = 'Z';

    // tile highlight
    public static final int UNMARKED = -1;;

    // tile visibility
    public static final int UNKNOWN = -1;
    public static final int UNSEEN = 0;

    // unit/building/tile noise
    public static final int MAX_NOISE = 10;

    // building status
    public static final int OVERRUN = -1;
    public static final int AWAITING_REPAIR = 0;
    public static final int READY_TO_USE = 1;

    // unit profession
    public static final char NONE = 'N';
    public static final char SOLDIER = 'S';
    public static final char BUILDER = 'B';
    public static final char FARMER = 'F';
    public static final char SCAVENGER = 's';
    public static final char NA = 0;

    // base training
    public static final int BASE_MAX = 3;
    public static final int TRAINING_TIME = 3;

    // build/unbuild options
    public static final char BUILD_WALL = 'w';
    public static final char BUILD_DIRT_ROAD = 'r';
    public static final char BUILD_FERTILE_LAND = 'l';
    public static final char BUILD_BASE = 'b';
    public static final char BUILD_TOWER = 't';
    public static final char BUILD_HOUSE = 'h';
    public static final char BUILD_FARM = 'f';
    public static final char BUILD_WELL = 'e';
    public static final char BUILD_BUILDING = 'u';
    public static final char BUILD_WAREHOUSE = 'a';
    public static final char UNBUILD_ROAD = 'R';
    public static final char UNBUILD_FOREST = 'F';
}
