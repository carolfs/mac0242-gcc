package buildings;

import global.Game;
import global.Global;
import mapping.Tile;
import units.*;

/**
 * Base militar, construção cuja principal função é treinar leigos.
 * @see Commoner
 */
public class Base extends Building
{
    /**
     * Leigos em treinamento na base militar.
     */
    private Commoner[] inTraining = new Commoner[Global.BASE_MAX];
    /**
     * Profissão para as quais os leigos estão treinando.
     */
    private char[] profession = new char[Global.BASE_MAX];
    /**
     * Turnos que faltam para que o leigo termine seu treinamento.
     */
    private int[] turnsRemaining = new int[Global.BASE_MAX];
    
    /**
     * Tamanho da construção (em quantidade de material, tempo necessário de construção)
     */
    public static final int SIZE = 200;
    
    @Override
    public int getSize() {
        return SIZE;
    }

    /**
     * Constroi uma base militar.
     * @param position lajota do mapa em que a base militar se encontra
     */
    public Base(Tile position)
    {
        status = Global.READY_TO_USE;
        maxResistance = resistance = 300;

        foodCapacity = 500;
        waterCapacity = 500;
        ammoCapacity = 5000;
        materialsCapacity = 500;
        housingCapacity = 10;

        this.position = position;

        for (int i = 0; i < Global.BASE_MAX; i++)
        {
            inTraining[i] = null;
            profession[i] = 0;
            turnsRemaining[i] = -1;
        }
    }

    /**
     * Atualiza o treinamento de leigos na virada do turno.
     */
    @Override
    public void startTurn()
    {
        //if (canTrain < Global.BASE_MAX)
            for (int i = 0; i < Global.BASE_MAX; i++)
            {
                if (turnsRemaining[i] == 1)
                {
                    this.finishTraining(i);
                    continue;
                }
                if (turnsRemaining[i] > 1)
                    turnsRemaining[i]--;
            }
    }

    /**
     * Inicia o treinamento de um leigo.
     * @param unit o leigo a ser treinado
     * @param profession a profissão para a qual o leigo será treinado
     * @return o leigo foi ou não aceito para o treinamento,
     * dependendo do número de unidades que já estão treinando
     */
    public boolean train(Commoner unit, char profession)
    {
        // TODO: diminuir steps e melee de unit para 0
        for (int i = 0; i < Global.BASE_MAX; i++)
            if(inTraining[i] == null)
            {
                inTraining[i] = unit;
                this.profession[i] = profession;
                turnsRemaining[i] = Global.TRAINING_TIME;
                unit.setTrainingIndex(i);
                return true;
            }
        return false;
    }

    /**
     * Finaliza o treinamento de um leigo
     * @param index a posição do leigo nos vetores
     */
    private void finishTraining(int index)
    {
        Commoner commoner = inTraining[index];
        position.removeUnit(commoner);
        global.Game.getInstance().removeUnit(commoner);
        Unit unit;
        switch (profession[index])
        {
            case Global.SOLDIER:
                unit = new Soldier(position);
                break;

            case Global.BUILDER:
                unit = new Builder(position);
                break;

            case Global.SCAVENGER:
                unit = new Scavenger(position);
                break;

            case Global.FARMER:
            default:
                unit = new Farmer(position);
                break;
        }

        global.Game.getInstance().newUnit(unit);
        position.addUnit(unit);
        unit.receiveResources(commoner);

        inTraining[index] = null;
        profession[index] = 0;
        turnsRemaining[index] = -1;

        //canTrain++;
        gui.CommonerView cv = (gui.CommonerView) commoner.getView();
        cv.finishedTraining(unit);
    }

    public void cancel(int index)
    {
        Commoner commoner = inTraining[index];

        inTraining[index] = null;
        profession[index] = 0;
        turnsRemaining[index] = -1;
    }

    /**
     * Retorna quantos turnos faltam para que uma unidade termine o seu treinamento.
     * @return o número de turnos
     */
    public int getTurnsRemaining(Commoner c) {
        return turnsRemaining[c.getTrainingIndex()];
    }
    
    public boolean canTrain(Commoner c)
    {
        return !c.isTraining() && getStatus() == global.Global.READY_TO_USE;
    }
    
    public boolean canMakeAmmo() {
        return (Game.getInstance().getMaterials() >= 10 &&
                Game.getInstance().getAmmo() < Game.getInstance().getMaxAmmo());
    }

    /**
     * Constroi munição a partir de materiais.
     */
    public void makeAmmo()
    {
        if (canMakeAmmo())
        {
            Game.getInstance().addMaterials(-10);
            Game.getInstance().addAmmo(1000);
        }
    }
}
