package buildings;

import global.Game;
import global.Global;
import java.util.Random;
import mapping.Tile;
import units.Soldier;

/**
 * Torre, construção cujo papel é abrigar um soldado, aumentando seu poder em
 * um ataque à distância e protegendo-o.
 */
public class Tower extends Building
{
    private int maxRanged = 0;
    private int ranged = 0;
    private int rangedDamage = 0;
    private int range = 0;

    private Soldier shooter;

    /**
     * Cria uma torre.
     * @param position lajota onde está a torre
     */
    public Tower(Tile position)
    {
        ammoCapacity = 1000;
        status = Global.READY_TO_USE;
        maxResistance = resistance = 50;

        ranged = maxRanged = 5;
        rangedDamage = 50;
        range = 3;

        this.position = position;
    }
    
    /**
     * Tamanho da construção (em quantidade de material, tempo necessário de construção)
     */
    public static final int SIZE = 40;
    
    @Override
    public int getSize() {
        return SIZE;
    }
    
    public boolean canDoRangedAttack()
    {
        return (getRanged() > 0 &&
                Game.getInstance().getAmmo() >= rangedDamage / 10);
    }

    /**
     * Faz um ataque à distância.
     * @param target unidade a ser atacada
     * @return sucesso no ataque
     */
    public boolean rangedAttack(units.Unit target)
    {
        if (target == null ||
            ranged <= 0 ||
            !hasShooter())
            return false;

        if (!Game.getInstance().addAmmo(-rangedDamage / 10))
            return false;
        ranged -= 1;

        double attacker = getResistance() * rangedDamage;
        double defender = target.getHealth() * target.getDefence();

        if (new Random().nextDouble() < attacker / (attacker + defender))
        {
            target.damage(rangedDamage);
            if (!target.isAlive())
                target.die();
            return true;
        }
        else
            return false;
    }

    /**
     * Recebe um soldado em seu interior.
     * @param shooter soldado (atirador) a ser abrigado
     */
    public void unitIn(Soldier shooter)
    {
        this.shooter = shooter;
        shooter.setTower(this);
        mapping.Map.getInstance().makeNoiseAndView(position, shooter.getNoise() + 2, shooter.getVision() + 2);
    }

    /**
     * Retira o soldado de seu interior.
     */
    public void unitOut()
    {
        if (shooter == null)
            return;

        shooter.setTower(null);
        mapping.Map.getInstance().takeNoiseAndView(position, shooter.getNoise() + 2, shooter.getVision() + 2);
        shooter = null;
    }

    /**
     * Retorna se há um atirador (soldado) no interior da torre
     */
    public boolean hasShooter()
    {
        return (shooter != null);
    }

    /**
     * Retorna o alcance do ataque á distância
     */
    public int getRange() {
        return range;
    }

    /**
     * Retorna o número de ataques à distância que o soldado na torre pode realizar
     * naquele turno
     */
    public int getRanged() {
        return ranged;
    }

    @Override
    public void startTurn()
    {
        ranged = maxRanged;
    }
}
