package buildings;

import global.Global;
import mapping.Tile;

/**
 * Casa, cujo papel é aumentar o número de habitações.
 */
public class House extends Building
{
    /**
     * Cria uma casa
     * @param position lajota onde a casa será construída
     */
    public House(Tile position)
    {
        status = Global.READY_TO_USE;
        maxResistance = resistance = 80;
        housingCapacity = 5;

        this.position = position;
    }
    
    /**
     * Tamanho da construção (em quantidade de material, tempo necessário de construção)
     */
    public static final int SIZE = 30;
    
    @Override
    public int getSize() {
        return SIZE;
    }
}
