package buildings;

import global.Global;
import mapping.Tile;

/**
 * Fazenda, onde um fazendeiro pode trabalhar, produzindo comida a custo de água.
 * @see units.Farmer
 */
public class Farm extends Building
{
    /**
     * Indica se a fazenda está sendo utilizada.
     */
    private boolean farmed = false;
    /**
     * Cria uma fazenda.
     * @param position lajota onde se encontra a fazenda
     */
    public Farm(Tile position)
    {
        status = Global.READY_TO_USE;
        maxResistance = resistance = 80;

        this.position = position;
    }
    
    /**
     * Tamanho da construção (em quantidade de material, tempo necessário de construção)
     */
    public static final int SIZE = 30;
    
    @Override
    public int getSize() {
        return SIZE;
    }

    /**
     * Determina se a fazenda aceita um novo fazendeiro.
     * @return se a fazenda aceita um novo fazendeiro
     */
    public boolean admitsNewFarmer() {
        return !farmed && getStatus() == global.Global.READY_TO_USE;
    }

    public void setFarmed(boolean farmed) {
        this.farmed = farmed;
    }
}
