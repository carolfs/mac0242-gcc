package buildings;

import global.Global;
import mapping.Tile;

/**
 * Armazém para comida, água, munição e materiais de construção.
 */
public class Warehouse extends Building
{
    /**
     * Constroi um armazém.
     * @param position lajota onde o armazém está
     */
    public Warehouse(Tile position)
    {
        status = Global.READY_TO_USE;
        maxResistance = resistance = 100;

        foodCapacity = 1000;
        waterCapacity = 1000;
        ammoCapacity = 10000;
        materialsCapacity = 1000;
    }
    
    /**
     * Tamanho da construção (em quantidade de material, tempo necessário de construção)
     */
    public static final int SIZE = 50;
    
    @Override
    public int getSize() {
        return SIZE;
    }
}
