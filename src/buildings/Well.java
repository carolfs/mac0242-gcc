package buildings;

import global.Game;
import global.Global;
import mapping.Tile;

/**
 * Poço, para obtenção de água.
 */
public class Well extends Building
{
    /**
     * Constroi um poço.
     * @param position lajota onde o poço está
     */
    public Well(Tile position)
    {
        status = Global.READY_TO_USE;
        maxResistance = resistance = 50;
    }
    
    /**
     * Tamanho da construção (em quantidade de material, tempo necessário de construção)
     */
    public static final int SIZE = 20;
    
    @Override
    public int getSize() {
        return SIZE;
    }

    /**
     * Recupera-se de ataques e produz água na virada do turno.
     */
    @Override
    public void startTurn()
    {
        if (getResistance() < maxResistance &&
            status == Global.READY_TO_USE)
            resistance += 10;

        if (status == Global.READY_TO_USE)
            Game.getInstance().addWater(10);
    }
}

