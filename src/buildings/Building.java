package buildings;

import global.*;
import java.util.Random;
import mapping.Tile;

/**
 * Descreve uma construção.
 */
abstract public class Building
{
    /**
     * Resistência máxima da construção, valor para o qual a resistência retorna
     * gradualmente conforme a construção se recupera de um ataque.
     */
    protected int maxResistance = 0;
    /**
     * Número de zumbis necessários para infestar a construção.
     */
    protected int resistance = 0;

    /**
     * Determina o status da construção:
     * -1 = infestada
     * 0 = aguardando reparos
     * 1 = pronta para uso
     */
    protected int status = Global.OVERRUN;

    /**
     * Lajota na qual a construção se encontra.
     */
    protected Tile position = null;
    
    /**
     * Armazena referência para o objeto da classe Game.
     */
    protected Game game = Game.getInstance();
    /**
     * Capacidade de armazenamento de comida.
     */
    protected int foodCapacity = 0;
    /**
     * Capacidade de armazenamento de água.
     */
    protected int waterCapacity = 0;
    /**
     * Capacidade de armazenamento de munição.
     */
    protected int ammoCapacity = 0;
    /**
     * Capacidade de armazenamento de materiais.
     */
    protected int materialsCapacity = 0;
    /**
     * Capacidade de habitação.
     */
    protected int housingCapacity = 0;
    /**
     * Contém material a ser saqueado.
     */
    protected boolean loot = true;

    /**
     * Avança a construção para o próximo turno. Esta versão do método não
     * faz nada.
     */
    public void startTurn(){}

    /**
     * Recebe o sinal de que a construção foi atacada.
     * @param attack força do ataque, a ser deduzida da resistência
     */
    public void beAttacked(int attack)
    {
        resistance -= attack;
        if (getResistance() <= 0)
        {
            Game.getInstance().loseBuilding(this);
            status = Global.OVERRUN;
        }
    }

    public int getAmmoCapacity()
    {
        return ammoCapacity;
    }

    public int getFoodCapacity()
    {
        return foodCapacity;
    }

    public int getHousingCapacity()
    {
        return housingCapacity;
    }

    public int getMaterialsCapacity()
    {
        return materialsCapacity;
    }

    public int getWaterCapacity()
    {
        return waterCapacity;
    }

    public int getStatus()
    {
        return status;
    }

    public boolean hasLoot()
    {
        return loot;
    }

    public void toggleLoot()
    {
        if (loot)
            loot = false;
    }

    public boolean isDamaged()
    {
        return (resistance < maxResistance);
    }

    public void restore()
    {
        if (status == Global.AWAITING_REPAIR)
        {
            status = Global.READY_TO_USE;
            resistance += maxResistance / 2;
            Game.getInstance().addBuilding(this);

            Game.getInstance().addAmmo(new Random().nextInt(ammoCapacity));
            Game.getInstance().addFood(new Random().nextInt(foodCapacity));
            Game.getInstance().addMaterials(new Random().nextInt(materialsCapacity));
            Game.getInstance().addWater(new Random().nextInt(waterCapacity));
        }

        if (resistance < maxResistance)
            resistance += 10;
        if (resistance > maxResistance)
            resistance = maxResistance;
    }

    public void clear()
    {
        if (status == Global.OVERRUN)
            status = Global.AWAITING_REPAIR;
    }

    public int getResistance()
    {
        return resistance;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public Tile getPosition()
    {
        return position;
    }

    /**
     * Determina o tamanho da construção, ou seja,
     * o número de turnos necessários para criá-la,
     * bem como a quantidade de material necessário.
     * @return tamanho da construção
     */
    abstract public int getSize();
}
