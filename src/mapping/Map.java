package mapping;

import global.Game;
import global.Global;
import java.io.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import units.*;

/**
 * A classe Map descreve um mapa de jogo.
 */
public class Map
{
    /**
     * Número do turno no jogo.
     */
    private int turn = 0;
    /**
     * Descreve a altura do mapa.
     */
    private int height;
    /**
     * Descreve a largura do mapa.
     */
    private int width;
    /**
     * Contém as 'height * width' lajotas do mapa.
     */
    private Tile grid[][];
    /**
     * Objeto singleton.
     */
    static private Map mapinstance = new Map("mapfile.map");
    /**
     * Retorna o mapa do jogo.
     * @return o objeto Map
     */
    static public Map getInstance() { return mapinstance; }

    /**
     * Cria um mapa de dada largura/altura.
     * @param width a largura do mapa (em número de lajotas)
     * @param height a altura do mapa (em número de lajotas)
     */
    private Map(int width, int height)
    {
        this.height = height;
        this.width = width;

        grid = new Tile[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                grid[i][j] = new Tile(i, j, this);
    }
    
    /**
     * Cria um mapa a partir de um arquivo.
     * @param filename o nome do arquivo
     */
    private Map(String filename)
    {
        InputStream file = null;
        try
        {
            file = getClass().getResourceAsStream(filename);
            DataInputStream input = new DataInputStream(file);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(input));

            height = Integer.parseInt(buffer.readLine());
            width = Integer.parseInt(buffer.readLine());
            grid = new Tile[width][height];

            for (int j = 0; j < width; j++)
                for (int i = 0; i < height; i++)
                    grid[i][j] = new Tile(i, j, this, buffer.readLine());
            
        } catch (FileNotFoundException fnf)
        {
            System.out.println("404 - File not found: " + filename);
            System.out.println(fnf);

        } catch (IOException io)
        {
            System.out.println("No line to read: " + io);
        } finally
        {
            try
            {
                file.close();
            } catch (IOException ex)
            {
                System.out.println("file.close() threw exception: " + ex);
            }
        }
    }

    /**
     * Inicia a virada de turno
     */
    public void startTurn()
    {
        for (Unit unit: Game.getInstance().getUnits())
            unit.resetForTurn();
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                grid[i][j].startTurn();
    }

    /**
     * Devolve a lajota a sudeste de outra
     * @param origin uma lajota
     * @return a lajota a sudeste da lajota dada
     */
    public Tile southeast(Tile origin)
    {
        if (origin.getY() == height - 1)
            return null;

        if (origin.getY() % 2 == 1)
        {
            if (origin.getX() == width - 1)
                return null;
            
            return grid[origin.getX() + 1][origin.getY() + 1];
        }

        return grid[origin.getX()][origin.getY() + 1];
    }

    /**
     * Devolve a lajota a leste de outra
     * @param origin uma lajota
     * @return a lajota a leste da lajota dada
     */
    public Tile east(Tile origin)
    {
        if (origin.getX() == width - 1)
            return null;

        return grid[origin.getX() + 1][origin.getY()];
    }

    /**
     * Devolve a lajota a nordeste de outra
     * @param origin uma lajota
     * @return a lajota a nordeste da lajota dada
     */
    public Tile northeast(Tile origin)
    {
        if (origin.getY() == 0)
            return null;

        if (origin.getY() % 2 == 1)
        {
            if (origin.getX() == width - 1)
                return null;

            return grid[origin.getX() + 1][origin.getY() - 1];
        }

        return grid[origin.getX()][origin.getY() - 1];
    }

    /**
     * Devolve a lajota a noroeste de outra
     * @param origin uma lajota
     * @return a lajota a noroeste da lajota dada
     */
    public Tile northwest(Tile origin)
    {
        if (origin.getY() == 0)
            return null;

        if (origin.getY() % 2 == 1)
        {
            return grid[origin.getX()][origin.getY() - 1];
        }

        if (origin.getX() == 0)
            return null;

        return grid[origin.getX() - 1][origin.getY() - 1];
    }

    /**
     * Devolve a lajota a oeste de outra
     * @param origin uma lajota
     * @return a lajota a oeste da lajota dada
     */
    public Tile west(Tile origin)
    {
        if (origin.getX() == 0)
            return null;

        return grid[origin.getX() - 1][origin.getY()];
    }

    /**
     * Devolve a lajota a sudoeste de outra
     * @param origin uma lajota
     * @return a lajota a sudoeste da lajota dada
     */
    public Tile southwest(Tile origin)
    {
        if (origin.getY() == height - 1)
            return null;

        if (origin.getY() % 2 == 1)
            return grid[origin.getX()][origin.getY() + 1];

        if (origin.getX() == 0)
            return null;

        return grid[origin.getX() - 1][origin.getY() + 1];
    }


    /**
     * Destaca uma área do mapa.
     * @param centre o centro da área a ser destacada
     * @param radius o raio da área a ser destacada
     * @param highlight o tipo de destaque
     */
    public void hlArea(Tile centre, int radius, int highlight)
    {
        if (centre == null)
            return;

        if (centre.highlight != highlight)
            centre.highlight = highlight;
        if (centre.flag != highlight)
            centre.flag = highlight;

        if (radius == 0)
            return;
        
        radius -= 1;
        hlArea(northeast(centre), radius, highlight);
        hlArea(east(centre), radius, highlight);
        hlArea(southeast(centre), radius, highlight);
        hlArea(southwest(centre), radius, highlight);
        hlArea(west(centre), radius, highlight);
        hlArea(northwest(centre), radius, highlight);
    }

    /**
     * Destaca a área do mapa para a qual uma unidade pode se mover.
     * @param centre o centro da área a ser destacada
     * @param steps quantos passos a unidade pode dar
     * @return se alguma lajota foi destacada
     */
    public boolean hlMoveable(Tile centre, int steps)
    {
        boolean hl = hlMoveable(centre, steps, 0);
        centre.highlight = Global.UNMARKED;
        return hl;
    }

    /**
     * Destaca a área do mapa para a qual uma unidade pode se mover.
     * @param centre o centro da área a ser destacada
     * @param steps quantos passos a unidade pode dar
     * @param highlight o tipo de destaque
     * @return se alguma lajota foi destacada
     */
    public boolean hlMoveable(Tile centre, int steps, int highlight)
    {
        boolean hl = false;

        if (centre == null ||
            centre.movementCost == Global.INFINITY ||
            centre.alignment == Global.ZOMBIE)
            return hl;

        if (centre.highlight == Global.UNMARKED ||
            centre.highlight > highlight)
        {
            centre.highlight = highlight;
            highlight += centre.movementCost;
            hl = true;
        }
        else
            return hl;

        if (steps <= 0)
            return hl;
        steps -= centre.getMovementCost();

        if (hlMoveable(northeast(centre), steps, highlight))
            hl = true;
        if (hlMoveable(east(centre), steps, highlight))
            hl = true;
        if (hlMoveable(southeast(centre), steps, highlight))
            hl = true;
        if (hlMoveable(southwest(centre), steps, highlight))
            hl = true;
        if (hlMoveable(west(centre), steps, highlight))
            hl = true;
        if (hlMoveable(northwest(centre), steps, highlight))
            hl = true;

        return hl;
    }

    /**
     * Remove o destaque à área do mapa para o qual uma unidade pode se mover.
     * @param centre o centro da área destacada
     * @param steps quantos passos a unidade pode dar
     */
    public void unHlMoveable(Tile centre, int steps)
    {
         if (centre == null ||
            centre.movementCost == Global.INFINITY ||
            centre.alignment == Global.ZOMBIE)
            return;

        centre.highlight = Global.UNMARKED;

        if (steps <= 0)
            return;

        steps -= centre.getMovementCost();
        unHlMoveable(northeast(centre), steps);
        unHlMoveable(east(centre), steps);
        unHlMoveable(southeast(centre), steps);
        unHlMoveable(southwest(centre), steps);
        unHlMoveable(west(centre), steps);
        unHlMoveable(northwest(centre), steps);
    }

    /**
     * Destaca a área do mapa na qual a unidade pode fazer um ataque.
     * @param centre o centro da área a ser destacada
     * @return se alguma lajota foi destacada
     */
    public boolean hlMelee(Tile centre)
    {
        boolean hl = false;

        if (northeast(centre) != null &&
            northeast(centre).alignment == Global.ZOMBIE)
        {
            northeast(centre).setHighlight(northeast(centre).getUnitCount());
            hl = true;
        }

        if (east(centre) != null &&
            east(centre).alignment == Global.ZOMBIE)
        {
            east(centre).setHighlight(east(centre).getUnitCount());
            hl = true;
        }

        if (southeast(centre) != null &&
            southeast(centre).alignment == Global.ZOMBIE)
        {
            southeast(centre).setHighlight(southeast(centre).getUnitCount());
            hl = true;
        }

        if (southwest(centre) != null &&
            southwest(centre).alignment == Global.ZOMBIE)
        {
            southwest(centre).setHighlight(southwest(centre).getUnitCount());
            hl = true;
        }

        if (west(centre) != null &&
            west(centre).alignment == Global.ZOMBIE)
        {
            west(centre).setHighlight(west(centre).getUnitCount());
            hl = true;
        }

        if (northwest(centre) != null &&
            northwest(centre).alignment == Global.ZOMBIE)
        {
            northwest(centre).setHighlight(northwest(centre).getUnitCount());
            hl = true;
        }

        return hl;
    }
    
    /**
     * Remove o destaque a uma área do mapa na qual uma unidade pode fazer um ataque.
     * @param centre o centro da área destacada
     */
    public void unHlMelee(Tile centre)
    {
        if (northeast(centre) != null)
            northeast(centre).setHighlight(Global.UNMARKED);

        if (east(centre) != null)
            east(centre).setHighlight(Global.UNMARKED);

        if (southeast(centre) != null)
            southeast(centre).setHighlight(Global.UNMARKED);

        if (southwest(centre) != null)
            southwest(centre).setHighlight(Global.UNMARKED);

        if (west(centre) != null)
            west(centre).setHighlight(Global.UNMARKED);

        if (northwest(centre) != null)
            northwest(centre).setHighlight(Global.UNMARKED);
    }
    
    /**
     * Destaca a área do mapa na qual uma unidade pode fazer um ataque à distância (atirar).
     * @param centre o centro da área a ser destacada
     * @param range o alcance do ataque da unidade
     * @return se alguma lajota foi destacada
     */
    public boolean hlRanged(Tile centre, int range)
    {
        return hlRanged(centre, range, 0);
    }

    /**
     * Destaca a área do mapa na qual uma unidade pode fazer um ataque à distância (atirar).
     * @param centre o centro da área a ser destacada
     * @param range o alcance do ataque da unidade
     * @param highlight o tipo de destaque
     * @return se alguma lajota foi destacada
     */
    public boolean hlRanged(Tile centre, int range, int highlight)
    {
        boolean hl = false;
        
        if (centre == null)
            return hl;

        if (centre.alignment == Global.ZOMBIE &&
            (centre.highlight == Global.UNMARKED ||
             centre.highlight > highlight))
        {
            centre.highlight = highlight;
            highlight += 1;
            hl = true;
        }

        if (range <= 0)
            return hl;

        range -= 1;
        if (hlRanged(northeast(centre), range, highlight))
            hl = true;
        if (hlRanged(east(centre), range, highlight))
            hl = true;
        if (hlRanged(southeast(centre), range, highlight))
            hl = true;
        if (hlRanged(southwest(centre), range, highlight))
            hl = true;
        if (hlRanged(west(centre), range, highlight))
            hl = true;
        if (hlRanged(northwest(centre), range, highlight))
            hl = true;

        return hl;
    }


    /**
     * Remove o destaque da área na qual uma unidade pode fazer um ataque à distância (atirar).
     * @param centre o centro da área destacada
     * @param range o alcance do ataque da unidade
     */
    public void unHlRanged(Tile centre, int range)
    {
        hlArea(centre, range, Global.UNMARKED);
    }

    /**
     * Adiciona barulho e visão das lajotas.
     * @param centre o centro do barulho/visão
     * @param noise a quantidade de barulho gerado
     * @param vision o alcance de visão
     */
    public void makeNoiseAndView(Tile centre, int noise, int vision)
    {
        mnV(centre, noise, vision);
        int radius = (noise > vision) ? noise : vision;
        hlArea(centre, radius, Global.UNMARKED);
    }

    private void mnV(Tile centre, int noise, int vision)
    {
        Queue<Tile> queue = new LinkedList<Tile>();
        Tile tile;

        centre.flag = 0;

        queue.offer(centre);

        while (!queue.isEmpty())
        {
            if ((tile = queue.poll()) == null ||
                 (tile.flag >= vision &&
                  tile.flag >= noise))
                continue;

            if (noise > tile.flag)
                tile.noise += noise - tile.flag;
            
            if (vision > tile.flag)
            {
                if (tile.visibility == Global.UNKNOWN)
                    tile.visibility = Global.UNSEEN;
                tile.visibility += 1;
            }


            if (northeast(tile) != null &&
                northeast(tile).flag == Global.UNMARKED)
            {
                northeast(tile).flag = tile.flag + 1;
                queue.offer(northeast(tile));
            }
            if (east(tile) != null &&
                east(tile).flag == Global.UNMARKED)
            {
                east(tile).flag = tile.flag + 1;
                queue.offer(east(tile));
            }
            if (southeast(tile) != null &&
                southeast(tile).flag == Global.UNMARKED)
            {
                southeast(tile).flag = tile.flag + 1;
                queue.offer(southeast(tile));
            }
            if (southwest(tile) != null &&
                southwest(tile).flag == Global.UNMARKED)
            {
                southwest(tile).flag = tile.flag + 1;
                queue.offer(southwest(tile));
            }
            if (west(tile) != null &&
                west(tile).flag == Global.UNMARKED)
            {
                west(tile).flag = tile.flag + 1;
                queue.offer(west(tile));
            }
            if (northwest(tile) != null &&
                northwest(tile).flag == Global.UNMARKED)
            {
                northwest(tile).flag = tile.flag + 1;
                queue.offer(northwest(tile));
            }
        }
    }

    /**
     * Remove barulho e visão das lajotas.
     * @param centre o centro do barulho/visão
     * @param noise a quantidade de barulho gerado
     * @param vision o alcance de visão
     */
    public void takeNoiseAndView(Tile centre, int noise, int vision)
    {
        tNV(centre, noise, vision);
        int radius = (noise > vision) ? noise : vision;
        hlArea(centre, radius, Global.UNMARKED);
    }

    private void tNV(Tile centre, int noise, int vision)
    {
        Queue<Tile> queue = new LinkedList<Tile>();
        Tile tile;

        centre.flag = 0;

        queue.offer(centre);

        while (!queue.isEmpty())
        {
            if ((tile = queue.poll()) == null ||
                 (tile.flag >= vision &&
                  tile.flag >= noise))
                continue;

            if (noise > tile.flag)
                tile.noise -= noise - tile.flag;
            if (vision > tile.flag)
            {
                tile.visibility -= 1;
                if (tile.visibility == Global.UNKNOWN)
                    tile.visibility = Global.UNSEEN;
            }


            if (northeast(tile) != null &&
                northeast(tile).flag == Global.UNMARKED)
            {
                northeast(tile).flag = tile.flag + 1;
                queue.offer(northeast(tile));
            }
            if (east(tile) != null &&
                east(tile).flag == Global.UNMARKED)
            {
                east(tile).flag = tile.flag + 1;
                queue.offer(east(tile));
            }
            if (southeast(tile) != null &&
                southeast(tile).flag == Global.UNMARKED)
            {
                southeast(tile).flag = tile.flag + 1;
                queue.offer(southeast(tile));
            }
            if (southwest(tile) != null &&
                southwest(tile).flag == Global.UNMARKED)
            {
                southwest(tile).flag = tile.flag + 1;
                queue.offer(southwest(tile));
            }
            if (west(tile) != null &&
                west(tile).flag == Global.UNMARKED)
            {
                west(tile).flag = tile.flag + 1;
                queue.offer(west(tile));
            }
            if (northwest(tile) != null &&
                northwest(tile).flag == Global.UNMARKED)
            {
                northwest(tile).flag = tile.flag + 1;
                queue.offer(northwest(tile));
            }
        }
    }

    /**
     * Remove barulho lajotas.
     * @param centre o centro do barulho/visão
     * @param noise a quantidade de barulho gerado
     */
    public void takeNoise(Tile centre, int noise)
    {
        Queue<Tile> queue = new LinkedList<Tile>();
        Tile tile;

        centre.flag = 0;

        queue.offer(centre);

        while (!queue.isEmpty())
        {
            if ((tile = queue.poll()) == null)
            {
                System.out.println("null");
                continue;
            }

            tile.noise -= noise - tile.flag;

            if (tile.noise < 0)
                tile.noise = 0;

            if (noise - tile.flag <= 1)
               continue;

            if (northeast(tile) != null &&
                northeast(tile).flag == Global.UNMARKED)
            {
                northeast(tile).flag = tile.flag + 1;
                queue.offer(northeast(tile));
            }
            if (east(tile) != null &&
                east(tile).flag == Global.UNMARKED)
            {
                east(tile).flag = tile.flag + 1;
                queue.offer(east(tile));
            }
            if (southeast(tile) != null &&
                southeast(tile).flag == Global.UNMARKED)
            {
                southeast(tile).flag = tile.flag + 1;
                queue.offer(southeast(tile));
            }
            if (southwest(tile) != null &&
                southwest(tile).flag == Global.UNMARKED)
            {
                southwest(tile).flag = tile.flag + 1;
                queue.offer(southwest(tile));
            }
            if (west(tile) != null &&
                west(tile).flag == Global.UNMARKED)
            {
                west(tile).flag = tile.flag + 1;
                queue.offer(west(tile));
            }
            if (northwest(tile) != null &&
                northwest(tile).flag == Global.UNMARKED)
            {
                northwest(tile).flag = tile.flag + 1;
                queue.offer(northwest(tile));
            }
        }
    }
    
    /**
     * Move uma unidade no mapa.
     * @param unit a unidade a ser movida
     * @param destination a lajota para a qual a unidade será movida
     */
    public void move(Unit unit, Tile destination)
    {       
        unit.goTo(destination, destination.getHighlight());
    }
    
    /**
     * @return the height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * @return the width
     */
    public int getWidth()
    {
        return width;
    }

    public Tile getGrid(int i, int j)
    {
        return grid[i][j];
    }

    /**
     * Retorna o número do turno no jogo.
     * @return número do turno
     */
    public int getTurn()
    {
        return turn;
    }

    /**
     * Avança para o próximo turno.
     */
    public void nextTurn()
    {
        turn += 1;
        if (turn == 500)
            Game.getInstance().youWin();
        startTurn();
    }

    /**
     * Adiciona caravanas e zumbis aleatoriamente ao jogo.
     */
    public void scatterAI()
    {
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
            {
                if (grid[i][j].getVisibility() <= Global.UNSEEN &&
                    grid[i][j].getMovementCost() != Global.INFINITY &&
                    new Random().nextFloat() < 0.1)
                {
                    Zombie zed = new Zombie(grid[i][j]);
                    grid[i][j].addUnit(zed);
                    Game.getInstance().newUnit(zed);
                }

                if (grid[i][j].getUnitCount() == 0 &&
                    grid[i][j].getMovementCost() != Global.INFINITY &&
                    new Random().nextFloat() < 0.01)
                {
                    Caravan survivors = new Caravan(grid[i][j]);
                    grid[i][j].addUnit(survivors);
                    Game.getInstance().newUnit(survivors);
                }
            }
    }
}
