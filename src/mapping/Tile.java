package mapping;

import units.Unit;
import buildings.*;
import global.Game;
import global.Global;
import java.util.ArrayList;
import java.util.Random;
import units.Caravan;
import units.Zombie;

/**
 * A classe Tile descreve uma lajota do mapa.
 * @see Map
 */
public class Tile
{
    /**
     *  Determina a visibilidade:
     *      -1 = não explorado
     *       0 = explorado e fora do campo de visão
     *       1 = dentro do campo de visão
     */
    protected int visibility;
    /**
     *  Indica se a lajota está destacada. O destaque indica em que lajotas uma ação
     *  (andar, atacar, construir etc) pode ser desempenhada. O número armazenado é
     *  a distância entre o centro do destaque e cada lajota destacada.
     */
    protected int highlight;

    protected int flag;
    /**
     *  Determina o terreno na lajota:
     *      p = planície (não altera defesa e movimento)
     *      h = morros (bonus para defesa, movimento mais caro)
     *      m = montanha (bonus para defesa, movimento mais caro)
     *      r = rio (proibida a ocupacao se não contiver uma ponte)
     */
    protected char terrain;
    /**
     *  Determina as estruturas presentes na lajota:
     *      -1 = ponte (permitida apenas em "rio".)
     *       0 = sem estruturas.
     *       1 = floresta (movimento mais caro, não pode ser criada)
     *       2 = estrada (bonus de movimento, não pode ser criada)
     *       4 = estrada de terra (bonus de movimento)
     *       8 = muro (impede a passagem de zumbis)
     *      16 = terra aravel (permite a construção de fazendas)
     */
    protected int structures;
    /**
     * Indica o custo de movimento, ou seja, quantos pontos de movimento
     * uma unidade gasta para se mover para a lajota.
     */
    protected int movementCost;
    /**
     * Indica o bonus multiplicativo que lajota dá para a unidade que a ocupa.
     */
    protected int defenceBonus;
    /**
     * Indica se há alguma construção na lajota. Apenas uma construção pode existir
     * por lajota.
     */
    protected Building building;
    /**
     * Indica as unidades presentes na lajota.
     */
    protected ArrayList<Unit> units = new ArrayList<Unit>();
    /**
     * Indica o nível de ruido na lajota. Mais unidades geram mais ruído. Zumbis se
     * movem na direção de crescimento do ruído.
     */
    protected int noise;
    /**
     *  Indica o alinhamento da lajota:
     *      H = humanos
     *      N = neutro (lajota vazia)
     *      Z = zumbis
     */
    protected char alignment;

    // Armazenam as coordenadas da lajota no mapa
    private int x;
    private int y;

    private mapping.Map map;

    /**
     * Construtor de lajota
     * @param x coordenada x da lajota no mapa
     * @param y coordenada y da lajota no mapa
     * @param map o mapa ao qual ela pertence
     */
    public Tile(int x, int y, mapping.Map map)
    {
        visibility = Global.UNKNOWN;
        highlight = Global.UNMARKED;
        flag = Global.UNMARKED;

        terrain = Global.PLAINS;
        structures = 0;
        calculateBonus();

        building = null;
        alignment = Global.NEUTRAL;

        this.x = x;
        this.y = y;
        this.map = map;
    }

    /**
     * Construtor de lajota
     * @param x coordenada x da lajota no mapa
     * @param y coordenada y da lajota no mapa
     * @param map o mapa ao qual ela pertence
     * @param seed determina como preencher a lajota (terreno, estruturas e
     * construções)
     */
    public Tile(int x, int y, mapping.Map map, String seed)
    {
        char[] kernel = seed.toCharArray();
        Random dice = new Random();
        
        visibility = Global.UNKNOWN;
        highlight = Global.UNMARKED;
        flag = Global.UNMARKED;

        switch (kernel[0])
        {
            case 'h':
                terrain = Global.HILL;
                break;
            case 'm':
                terrain = Global.MOUNTAIN;
                break;
            case 'p':
                terrain = Global.PLAINS;
                break;
            case 'r':
                terrain = Global.RIVER;
                break;
            case 'M':
                if (dice.nextFloat() < 0.75)
                    terrain = Global.MOUNTAIN;
                else
                    terrain = Global.HILL;
                break;
            case 'P':
                if (dice.nextFloat() < 0.75)
                    terrain = Global.PLAINS;
                else
                    terrain = Global.HILL;
                break;

            default:
                terrain = Global.PLAINS;
                break;
        }

        switch (kernel[1])
        {
            case 'b':
                structures = Global.BRIDGE;
                break;
            case 'd':
                structures = Global.DIRT_ROAD;
                break;
            case 'f':
                structures = Global.FOREST;
                break;
            case 'r':
                structures = Global.ROAD;
                break;
            case 'w':
                structures = Global.WALL;
                break;
            case 'F':
                if (terrain == Global.PLAINS &&
                    dice.nextFloat() < 0.5)
                    structures = Global.FOREST;
                else
                    structures = 0;
                break;

            case '-':
            default:
                structures = 0;
                break;
        }
        calculateBonus();

        switch (kernel[2])
        {
            case 'b':
                building = new Base(this);
                Game.getInstance().newBuilding(building);
                break;
            case 'f':
                building = new Farm(this);
                building.setStatus(Global.OVERRUN);
                structures += Global.FERTILE_LAND;
                Game.getInstance().newBuilding(building);
                break;
            case 'h':
                building = new House(this);
                building.setStatus(Global.OVERRUN);
                Game.getInstance().newBuilding(building);
                break;
            case 't':
                building = new Tower(this);
                Game.getInstance().newBuilding(building);
                break;
            case 'w':
                building = new Warehouse(this);
                building.setStatus(Global.OVERRUN);
                Game.getInstance().newBuilding(building);
                break;
            case 'W':
                building = new Well(this);
                building.setStatus(Global.OVERRUN);
                Game.getInstance().newBuilding(building);
                break;

            case '-':
            default:
                building = null;
                break;

        }
        alignment = Global.NEUTRAL;

        this.x = x;
        this.y = y;
        this.map = map;
    }

    public Tile()
    {
        visibility = Global.UNKNOWN;
        highlight = Global.UNMARKED;

        terrain = Global.PLAINS;
        structures = 0;
        calculateBonus();

        building = null;
    }

    /**
     * Utiliza os métodos 'calculateTerrainBonus' e 'calculateStructureBonus' para
     *  calcular o custo de movimento e o bonus de defesa da lajota.
     */
    private void calculateBonus()
    {
        calculateTerrainBonus();
        calculateStructureBonus();
    }

    /**
     * Calcula, a partir do atributo 'terrain', o custo de movimento e o bonus de
     *  defesa da lajota.
     */
    private void calculateTerrainBonus()
    {
        switch(terrain)
        {
            case Global.PLAINS:
                movementCost = 20;
                defenceBonus = 20;
                break;
            case Global.HILL:
                movementCost = 40;
                defenceBonus = 40;
                break;
            case Global.MOUNTAIN:
                movementCost = 60;
                defenceBonus = 60;
                break;
            case Global.RIVER:
                movementCost = Global.INFINITY;
                defenceBonus = 0;
                break;
            default:
                break;
        }
    }

    /**
     * Calcula, a partir do atributo 'structures', o custo de movimento e o bonus
     *  de defesa da lajota.
     */
    private void calculateStructureBonus()
    {
        int aux = structures;

        if (hasBridge())
        {
            movementCost = 10;
            return;
        }

        if (aux >= 4)
        {

            aux -= Global.DIRT_ROAD;
            if (movementCost > 10)
                movementCost *= 0.5;
        }

        if (aux >= 2)
        {
            aux -= Global.ROAD;
            movementCost = 10;
        }

        if (aux >= 1)
        {
            movementCost += 10;
            defenceBonus += 10;
        }
    }

    /**
     * Adiciona uma unidade à lajota.
     * @param unit unidade a ser adicionada
     */
    public void addUnit(Unit unit)
    {
        if (unit == null)
            return;

        if (units.isEmpty())
            alignment = unit.getAlignment();


        units.add(0, unit);

        if (unit.getAlignment() != Global.ZOMBIE)
            map.makeNoiseAndView(this, unit.getNoise(), unit.getVision());
    }

    /**
     * Remove uma unidade da lajota.
     * @param unit unidade a ser removida
     */
    public void removeUnit(Unit unit)
    {
        if (unit.getAlignment() != Global.ZOMBIE)
            map.takeNoiseAndView(this, unit.getNoise(), unit.getVision());

        units.remove(unit);
        
        if (getUnitCount() == 0)
            alignment = Global.NEUTRAL;
    }

    /**
     * Inicia a virada do turno.
     */
    public void startTurn()
    {
        highlight = Global.UNMARKED;
        for (int i = 0; i < getUnitCount(); i++)
            units.get(i).startTurn();
        if (building != null)
            building.startTurn();

        if (visibility > Global.UNSEEN ||
            terrain == Global.RIVER)
            return;

        float spawnChance = new Random().nextFloat();
        if (spawnChance > 0.5)
            return;

        if (spawnChance <= 0.05 &&
            (getUnitCount() == 0 ||
             alignment == Global.ZOMBIE))
        {
            Zombie zed = new Zombie(this);
            addUnit(zed);
            Game.getInstance().newUnit(zed);
        }

        if (spawnChance <= 0.2 &&
            (x == 0 ||
             x == map.getWidth() - 1 ||
             y == 0 ||
             y == map.getHeight() - 1) &&
             (getUnitCount() == 0 ||
             alignment == Global.ZOMBIE))
        {
            Zombie zed = new Zombie(this);
            addUnit(zed);
            Game.getInstance().newUnit(zed);
        }

        if (alignment != Global.NEUTRAL ||
            (x != 0 &&
             x != map.getWidth() - 1 &&
             y != 0 &&
             y != map.getHeight() - 1))
            return;

        if (spawnChance <= 0.01)
        {
            Caravan survivors = new Caravan(this);
            addUnit(survivors);
            Game.getInstance().newUnit(survivors);
        }
    }

    public int getVisibility()
    {
        return visibility;
    }

    public void setVisibility(int visibility)
    {
        this.visibility = visibility;
    }

    public Building getBuilding()
    {
        return building;
    }

    public void setBuilding(Building building)
    {
        if (this.building != null &&
            building == null)
            map.takeNoiseAndView(this, 1, -1);
        this.building = building;
        if (building != null)
            map.makeNoiseAndView(this, 1, -1);
    }

    public int getDefenceBonus()
    {
        return defenceBonus;
    }

    public void setDefenceBonus(int defenceBonus)
    {
        this.defenceBonus = defenceBonus;
    }

    public int getMovementCost()
    {
        return movementCost;
    }

    public void setMovementCost(int movementCost)
    {
        this.movementCost = movementCost;
    }

    public int getStructures()
    {
        return structures;
    }

    public void setStructures(int structures)
    {
        this.structures = structures;
        calculateBonus();
    }

    public char getTerrain()
    {
        return terrain;
    }

    public void setTerrain(char terrain)
    {
        this.terrain = terrain;
        calculateBonus();
    }

    public int getNoise() {
        return noise;
    }

    public void setNoise(int noise) {
        this.noise = noise;
    }

    public char getAlignment() {
        return alignment;
    }

    public int getHighlight()
    {
        return highlight;
    }

    public void setHighlight(int highlight)
    {
        this.highlight = highlight;
    }

    /**
     * @return the x
     */
    public int getX()
    {
        return x;
    }

    /**
     * @return the y
     */
    public int getY()
    {
        return y;
    }

    /**
     * @return the units
     */
    public ArrayList<Unit> getUnits()
    {
        return units;
    }
    
    /**
     * Põe a próxima unidade no topo da lista.
     */
    public void rotateUnits()
    {
        Unit u0 = units.get(0);
        units.remove(0);
        units.add(u0);
    }
    
    /**
     * Põe a unidade no topo da lista.
     * @param unit 
     */
    public void putOnTop(Unit unit)
    {
        if (units.remove(unit))
            units.add(0, unit);
    }
    
    /**
     * Retorna uma unidade da lajota
     * @param i índice da unidade
     * @return a unidade com o índice dado
     */
    public Unit getUnit(int i)
    {
        return units.get(i);
    }

    /**
     * Retorna a quantidade de unidades na lajota
     * @return a quantidade de unidades na lajota
     */
    public int getUnitCount()
    {
        return units.size();
    }

    public boolean hasBridge()
    {
        return (structures == -1);
    }

    public void addBridge()
    {
        if (!hasBridge())
        {
            structures = -1;
            calculateStructureBonus();
        }
    }

    public void removeBridge()
    {
        if (hasBridge())
        {
            structures = 0;
            calculateStructureBonus();
        }
    }

    public boolean isFertile()
    {
        return (structures >= 16);
    }

    public void addFertile()
    {
        if (!isFertile() && terrain == Global.PLAINS)
            structures += Global.FERTILE_LAND;
    }

    public void removeFertile()
    {
        if (isFertile())
            structures -= Global.FERTILE_LAND;
    }

    public boolean hasWall()
    {
        int aux = structures;
        if (isFertile())
            aux -= Global.FERTILE_LAND;

        return (aux >= 8);
    }

    public void addWall()
    {
        if (!(hasWall() || hasBridge()))
            structures += Global.WALL;
    }

    public void removeWall()
    {
        if (hasWall())
            structures -= Global.WALL;
    }

    public boolean hasDirtRoad()
    {
        int aux = structures;

        if (hasBridge())
            return false;

        if (isFertile())
            aux -= Global.FERTILE_LAND;
        if (hasWall())
            aux -= Global.WALL;

        return (aux >= 4);
    }

    public void addDirtRoad()
    {
        if (!(hasDirtRoad() || hasRoad() || hasBridge()))
        {
            structures += Global.DIRT_ROAD;
            calculateStructureBonus();
        }
    }

    public void removeDirtRoad()
    {
        if (hasDirtRoad())
        {
            structures -= Global.DIRT_ROAD;
            calculateStructureBonus();
        }
    }

    public boolean hasRoad()
    {
        int aux = structures;
        if (hasDirtRoad() || hasBridge())
            return false;

        if (isFertile())
            aux -= Global.FERTILE_LAND;
        if (hasWall())
            aux -= Global.WALL;

        return (aux >= 2);
    }

    public void addRoad()
    {
        if (!hasRoad())
        {
            structures += Global.ROAD;
            calculateStructureBonus();
        }
    }

    public void removeRoad()
    {
        if (hasRoad())
        {
            structures -= Global.ROAD;
            calculateStructureBonus();
        }
    }

    public boolean hasForest()
    {
        if (hasBridge())
            return false;

        return (structures % 2 == 1);
    }

    public void addForest()
    {
        if (!hasForest())
        {
            structures += Global.FOREST;
            calculateStructureBonus();
        }
    }

    public void removeForest()
    {
        if (hasForest())
        {
            structures -= Global.FOREST;
            calculateStructureBonus();
        }
    }
    
    public boolean isSeen()
    {
        return (visibility > 0);
    }
    
    public boolean isUnseen()
    {
        return (visibility == Global.UNSEEN);
    }
    
    public boolean isUnknown()
    {
        return (visibility == Global.UNKNOWN);
    }
    
    /**
     * Determina se é possível usar uma opção de construção na lajota (usada pela GUI).
     * @param buildOption a opção de construção
     * @return se é possível usar a opção aqui
     */
    public boolean allowsBuild(char buildOption)
    {
        switch(buildOption)
        {
            case Global.BUILD_WALL:
                if (hasWall() || hasBridge() || hasForest())
                    return false;
                else
                    return true;

            case Global.BUILD_DIRT_ROAD:
                if (hasBridge() || hasRoad() || hasDirtRoad() || hasForest())
                    return false;
                else
                    return true;
            
            case Global.BUILD_WELL:
            case Global.BUILD_BASE:
            case Global.BUILD_TOWER:
            case Global.BUILD_HOUSE:
            case Global.BUILD_WAREHOUSE:
                return (building == null && !hasBridge());
            
            case Global.BUILD_FERTILE_LAND:
                return (building == null && !hasRoad() && !hasDirtRoad()
                        && !hasForest() && !isFertile() && !hasBridge());

            case Global.BUILD_FARM:
                if (building == null && isFertile())
                    return true;
                else
                    return false;
                
            default:
                return false;
        }
    }
}