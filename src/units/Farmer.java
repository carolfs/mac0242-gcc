package units;

import buildings.*;
import global.Game;
import global.Global;
import mapping.Tile;

/**
 * Fazendeiro. Constroi fazendas e poços, trabalha em fazendas.
 * @see Unit
 */
public class Farmer extends Unit
{
    buildings.Farm farm;
    private boolean isBuilding = false;
    private int turnsLeft = -1;
    private Building building = null;


    public Farmer(Tile position)
    {
        health = maxHealth = 80;

        maxMaterials = materials = 80;
        maxWater = water = 25;
        maxFood = food = 25;

        melee = maxMelee = 1;
        meleeDamage = 50;

        defence = 2;

        steps = maxSteps = 30;

        vision = 2;

        noise = 3;

        selected = false;

        alignment = Global.HUMAN;

        this.position = position;

        view = new gui.FarmerView(this);
    }

    public void build(char option)
    {
        switch(option)
        {
            case Global.BUILD_FERTILE_LAND:
                position.addFertile();
                break;

            case Global.BUILD_FARM:
                if (position.getBuilding() == null)
                    building = new Farm(position);
                break;
            case Global.BUILD_WELL:
                if (position.getBuilding() == null)
                    building = new Well(position);
                break;
            default:
                return;
        }

        if (building != null &&
            building.getSize() <= materials)
        {
            turnsLeft = getBuilding().getSize() / 10;
            isBuilding = true;
        }
    }
    
    public boolean canBuild(char option)
    {
        switch(option)
        {
            case Global.BUILD_FERTILE_LAND:
                return true;

            case Global.BUILD_FARM:
                return materials >= Farm.SIZE;
            case Global.BUILD_WELL:
                return materials >= Well.SIZE;
            default:
                return false;
        }
    }

    public void finishBuilding()
    {
        position.setBuilding(getBuilding());
        Game.getInstance().newBuilding(getBuilding());
        materials -= getBuilding().getSize();
        isBuilding = false;
        building = null;
        turnsLeft = -1;
    }

    public void cancel()
    {
        if (isBuilding)
        {
            isBuilding = false;
            building = null;
            turnsLeft = -1;
        }

        if (isFarming())
        {
            farm = null;
            buildings.Farm farm = (buildings.Farm) position.getBuilding();
            farm.setFarmed(false);
        }
    }


    @Override
    public void startTurn()
    {
        if (isFarming())
        {
            Game.getInstance().addFood(5);
            Game.getInstance().addWater(-5);
            return;
        }

        if (food > 0)
        {
            food -= 1;
            health += 5;
        }
        else
            health -= 1;

        if (water > 0)
        {
            water -= 1;
            health += 5;
        }
        else
            health -= 1;

        if (health > maxHealth)
                health = maxHealth;

        if (!isAlive())
            die();

        if (isBuilding)
        {
            turnsLeft -= 1;
            if (turnsLeft == 0)
                finishBuilding();
            else
                return;
        }
        ranged = maxRanged;
        melee = maxMelee;
        steps = maxSteps;
        selected = false;
        replenish();
    }

    public void farm()
    {
        if (position.getBuilding() instanceof buildings.Farm &&
            Game.getInstance().getWater() >= 5)
        {
            melee = 0;
            steps = 0;
            farm = (buildings.Farm) position.getBuilding();
            farm.setFarmed(true);
        }
    }
    
    /**
     * @return the isBuilding
     */
    public boolean getIsBuilding()
    {
        return isBuilding;
    }
    
    public boolean isFarming()
    {
        return farm != null;
    }

    /**
     * @return the turnsLeft
     */
    public int getTurnsLeft()
    {
        return turnsLeft;
    }

    public Building getBuilding()
    {
        return building;
    }
    
    @Override
    public boolean isAwaitingCommand() {
        return super.isAwaitingCommand() && !getIsBuilding() && !isFarming();
    }
}
