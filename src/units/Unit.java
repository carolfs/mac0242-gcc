package units;

import global.Game;
import global.Global;
import java.util.Random;
import mapping.Tile;

/**
 * A classe mãe Unit descreve uma unidade controlada pelo jogador.
 */
public class Unit
{
    /**
     * Pontos de vida totais da unidade. Caso se esgotem, a unidade morre.
     */
    protected int maxHealth = 0;

    /**
     * Pontos de vida restantes da unidade. Varia entre 0 e 'maxHealth'.
     */
    protected int health = 0;

    /**
     * Quantidade de comida que uma unidade pode transportar. Caso a comida acabe,
     * a unidade começa a perder um ponto de vida a cada turno.
     */
    protected int maxFood = 0;
    /**
     * Quantidade de comida que a unidade transporta. Varia entre 0 e 'maxFood'.
     */
    protected int food = 0;
    protected int maxWater = 0;
    protected int water = 0;

    protected int materials = 0;
    protected int maxMaterials = 0;

    /**
     * Número máximo de vezes que uma unidade pode atacar à distância em um turno.
     */
    protected int maxRanged = 0;
    /**
     * Número de vezes que a unidade ainda pode atacar à distância neste turno.
     * Varia entre 0 e 'maxRanged'.
     */
    protected int ranged = 0;
    /**
     * Pontos de ataque à distância da unidade. Representa o dano causado em um ataque.
     */
    protected int rangedDamage = 0;
    /**
     * Distancia máxima entre a unidade e um alvo possível. Varia entre 1 e 'vision'.
     */
    protected int range = 0;
    /**
     * Indica a quantidade de munição que uma unidade pode carregar. Quando a munição
     * se esgota, a unidade não pode mais atacar à distância.
     */
    protected int maxAmmo = 0;
    /**
     * Indica a quantidade de munição que uma unidade tem disponível. Varia entre 0
     * e 'maxAmmo'.
     */
    protected int ammo = 0;
    
    /**
     * Número máximo de ataques corpo-a-corpo que a unidade pode fazer.
     */
    protected int maxMelee = 0;
    /**
     * Número de ataques corpo-a-corpo que a unidade ainda pode fazer neste turno.
     * Varia entre 0 e 'maxMelee'.
     */
    protected int melee = 0;
    protected int meleeDamage = 0;

    /**
     * Pontos de defesa da unidade. Uma unidade atacada por um inimigo com A pontos
     * de ataque perde A - 'defence' pontos de vida.
     */
    protected int defence = 0;
    /**
     * Determina se uma unidade foi atacada por zumbis.
     */
    private boolean attackedByZombies = false;

    /**
     * Número máximo de passos que uma unidade pode dar por turno. Um passo é
     * definido como a distância manhattan entre uma lajota e suas vizinhas diretas.
     */
    protected int maxSteps;
    /**
     * Número de passos que a unidade ainda pode dar no presente turno. Varia entre
     * 0 e 'maxSteps'.
     */
    protected int steps;

    /**
     * Distância a que uma unidade enxerga.
     */
    protected int vision = 0;
    /**
     * Ruído que uma unidade causa. Decresce linearmente com a distância.
     */
    protected int noise = 0;

    /**
     * Determina se uma unidade está selecionada.
     */
    protected boolean selected = false;

    protected char alignment = Global.NEUTRAL;

    /**
     * Lajota onde está a unidade no mapa.
     */
    protected Tile position = null;
    protected Game game = Game.getInstance();

    /**
     * View responsável pela exibição desta unidade no mapa. Será de uma subclasse
     * específica para cada tipo de unidade.
     */
    protected gui.UnitView view = null;

    public Unit() {}

    /**
     * Executa tarefas para a virada de turno.
     */
    public void startTurn()
    {
        ranged = maxRanged;
        melee = maxMelee;
        steps = maxSteps;
        selected = false;

        if (food > 0)
        {
            food -= 1;
            health += 5;
        }
        else
            health -= 5;

        if (water > 0)
        {
            water -= 1;
            health += 5;
        }
        else
            health -= 15;
        
        if (health > maxHealth)
                health = maxHealth;

        if (!isAlive()) {
            die();
            return;
        }


        replenish();
    }

    /**
     * Repõe a saúde, comida, água, munição e materiais da unidade se ela
     * não é humana.
     */
    public void replenish()
    {
        if (position.getBuilding() != null &&
            position.getAlignment() == Global.HUMAN)
        {
            if (Game.getInstance().addFood(food - maxFood))
                food = maxFood;

            if (Game.getInstance().addWater(water - maxWater))
                water = maxWater;

            if (Game.getInstance().addAmmo(ammo - maxAmmo))
                ammo = maxAmmo;

            if (Game.getInstance().addMaterials(materials - maxMaterials))
                materials = maxMaterials;
            
            if (food > 0 && water > 0) {
                health += 10;
                if (health > maxHealth)
                    health = maxHealth;
            }
        }
    }
    
    public boolean isAlive()
    {
        return (health > 0);
    }

    /**
     * Retira a unidade more do jogo, criando um zumbi no lugar se ela for humana.
     */
    public void die()
    {
        Game.getInstance().removeUnit(this);
        position.removeUnit(this);
        
        if (alignment == Global.HUMAN)
        {
            Zombie deadGuy = new Zombie(position);
            position.addUnit(deadGuy);
            Game.getInstance().newUnit(deadGuy);
        }
    }

    /*
     * Ações comuns a todas as unidades
     */

    /**
     * Ataca outra unidade.
     * @param target a unidade a ser atacada
     */
    public void meleeAttack(Unit target)
    {
        if (target == null || melee <= 0)
            return;

        double attacker = getHealth() * meleeDamage;
        double defender = target.getHealth() * target.defence;
        melee -= 1;

        if (new Random().nextDouble() < attacker / (attacker + defender))
        {
            target.health -= meleeDamage;
            if (!target.isAlive())
                target.die();
            return;
        }
        else
        {
            health -= meleeDamage;
            if (!isAlive())
                die();
            return;
        }
    }

    /**
     * Move a unidade para outra lajota.
     * @param destination a lajota de destino
     * @param distance a distância a ser percorrida pela unidade
     */
    public void goTo(Tile destination, int distance)
    {
        if (destination == null)
            return;

        position.removeUnit(this);

        destination.addUnit(this);

        position = destination;

        steps -= distance;
    }
    
    /*
     * Getters e Setters
     */
    public Tile getPosition()
    {
        return position;
    }

    public void setPosition(Tile position)
    {
        this.position = position;
    }

    public int getSteps()
    {
        return steps;
    }

    public void setSteps(int steps)
    {
        this.steps = steps;
    }

    public int getNoise()
    {
        return noise;
    }

    public void setNoise(int noise)
    {
        this.noise = noise;
    }

    public char getAlignment()
    {
        return alignment;
    }

    public int getVision()
    {
        return vision;
    }

    public gui.UnitView getView()
    {
        return view;
    }

    /**
     * @return the range
     */
    public int getRange() {
        return range;
    }

    /**
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    public void damage(int damage)
    {
        health -= damage;
    }


    /**
     * @return the ranged
     */
    public int getRanged()
    {
        return ranged;
    }

    public int getDefence()
    {
        return defence;
    }
    
     
    /**
    * Recebe os recursos de uma unidade.
    * @param unit a unidade que vai doar os recursos
    */
    public void receiveResources(Unit unit)
    {
        this.ammo = unit.ammo;
        if (this.ammo > this.maxAmmo) {
            Game.getInstance().addAmmo(this.ammo - this.maxAmmo);
            this.ammo = this.maxAmmo;
        }
        this.materials = unit.materials;
        if (this.materials > this.maxMaterials) {
            Game.getInstance().addMaterials(this.materials - this.maxMaterials);
            this.ammo = this.maxAmmo;
        }
        this.food = unit.food;
        if (this.food > this.maxFood) {
            Game.getInstance().addFood(this.food - this.maxFood);
            this.food = this.maxFood;
        }
        this.water = unit.water;
        if (this.water > this.water) {
            Game.getInstance().addMaterials(this.water - this.maxWater);
            this.water = this.maxWater;
        }
    }

    /**
     * @return the melee
     */
    public int getMelee() {
        return melee;
    }
    
    /**
     * Função utilizada pela GUI para saber se a unidade pode receber novos
     * comandos além de "cancel".
     * @return se ela pode receber novos comandos
     */
    public boolean isAwaitingCommand() {
        return getAlignment() == Global.HUMAN && (getMelee() > 0 || getSteps() > 0);
    }

    /**
     * @return the attackedByZombies
     */
    public boolean wasAttackedByZombies() {
        return attackedByZombies;
    }

    /**
     * Inicializa o estado de uma unidade para o início do turno.
     */
    public void resetForTurn() {
        this.attackedByZombies = false;
    }

    /**
     * @param attackedByZombies the attackedByZombies to set
     */
    public void setAttackedByZombies(boolean attackedByZombies)
    {
        this.attackedByZombies = attackedByZombies;
    }

    /**
     * Pontos de vida totais da unidade. Caso se esgotem, a unidade morre.
     * @return the maxHealth
     */
    public int getMaxHealth() {
        return maxHealth;
    }

    /**
     * Quantidade de comida que uma unidade pode transportar. Caso a comida acabe,
     * a unidade começa a perder um ponto de vida a cada turno.
     * @return the maxFood
     */
    public int getMaxFood() {
        return maxFood;
    }

    /**
     * Quantidade de comida que a unidade transporta. Varia entre 0 e 'maxFood'.
     * @return the food
     */
    public int getFood() {
        return food;
    }

    /**
     * @return the maxWater
     */
    public int getMaxWater() {
        return maxWater;
    }

    /**
     * @return the water
     */
    public int getWater() {
        return water;
    }

    /**
     * @return the materials
     */
    public int getMaterials() {
        return materials;
    }

    /**
     * @return the maxMaterials
     */
    public int getMaxMaterials() {
        return maxMaterials;
    }

    /**
     * Indica a quantidade de munição que uma unidade pode carregar. Quando a munição
     * se esgota, a unidade não pode mais atacar à distância.
     * @return the maxAmmo
     */
    public int getMaxAmmo() {
        return maxAmmo;
    }

    /**
     * Indica a quantidade de munição que uma unidade tem disponível. Varia entre 0
     * e 'maxAmmo'.
     * @return the ammo
     */
    public int getAmmo() {
        return ammo;
    }
    
    /**
     * Indica se a unidade pode se mover neste turno.
     * @return se a unidade pode se mover
     */
    public boolean canMove() {
        return getSteps() > 0;
    }
    
    /**
     * Indica se a unidade pode atacar neste turno.
     * @return se a unidade pode atacar
     */
    public boolean canAttack() {
        return getMelee() > 0;
    }


}
