package units;

import global.*;
import java.util.Random;
import mapping.Tile;

/**
 * Um zumbi, vagando pela mapa, devorando cérebros.
 * @see Unit
 */
public class Zombie extends Unit
{
    int lastMove;

    /**
     * Constroi um zumbi
     * @param position lajota onde ele está no mapa
     */
    public Zombie(Tile position)
    {
        health = maxHealth = 50;
        defence = 1;

        melee = maxMelee = 1;
        meleeDamage = 30;

        alignment = Global.ZOMBIE;

        this.position = position;
        lastMove = mapping.Map.getInstance().getTurn();
        
        view = new gui.ZombieView(this);
    }

    /**
     * Determina para onde o zumbi se moverá
     * @param direction inteiro cujo resto por 6 determina uma direção
     * @return lajota de destino
     */
    private Tile getDestination(int direction)
    {
        Tile destination = null;

        if (direction == -1)
            direction = new Random().nextInt(6);

        while (destination == null)
        {
            switch(direction % 6)
            {
                case 0:
                    destination = mapping.Map.getInstance().east(position);
                    break;

                case 1:
                    destination = mapping.Map.getInstance().southeast(position);
                    break;

                case 2:
                    destination = mapping.Map.getInstance().southwest(position);
                    break;

                case 3:
                    destination = mapping.Map.getInstance().west(position);
                    break;

                case 4:
                    destination = mapping.Map.getInstance().northwest(position);
                    break;
                    
                case 5:
                    destination = mapping.Map.getInstance().northeast(position);
                    break;
            }

            if (destination == null)
                direction = new Random().nextInt(6);

            if (destination != null &&
                (destination.hasWall() ||
                 destination.getMovementCost() == Global.INFINITY))
            {
                destination = null;
                direction += 1;
                if (direction == 12)
                    return position;
            }
        }

        return destination;
    }

    /**
     * Determina a direção para a qual o zumbi de moverá, de acordo com o barulho
     * no ambiente.
     * @return um inteiro cujo resto por 6 determina uma direção
     */
    private int findTarget()
    {
        int maxNoise = position.getNoise();
        int direction = -1;

        if (mapping.Map.getInstance().east(position) != null &&
            mapping.Map.getInstance().east(position).getNoise() > maxNoise)
        {
            maxNoise = mapping.Map.getInstance().east(position).getNoise();
            direction = 0;
        }

        if (mapping.Map.getInstance().southeast(position) != null &&
            mapping.Map.getInstance().southeast(position).getNoise() > maxNoise)
        {
            maxNoise = mapping.Map.getInstance().southeast(position).getNoise();
            direction = 1;
        }

        if (mapping.Map.getInstance().southwest(position) != null &&
            mapping.Map.getInstance().southwest(position).getNoise() > maxNoise)
        {
            maxNoise = mapping.Map.getInstance().southwest(position).getNoise();
            direction = 2;
        }

        if (mapping.Map.getInstance().west(position) != null &&
            mapping.Map.getInstance().west(position).getNoise() > maxNoise)
        {
            maxNoise = mapping.Map.getInstance().west(position).getNoise();
            direction = 3;
        }

        if (mapping.Map.getInstance().northwest(position) != null &&
            mapping.Map.getInstance().northwest(position).getNoise() > maxNoise)
        {
            maxNoise = mapping.Map.getInstance().northwest(position).getNoise();
            direction = 4;
        }

        if (mapping.Map.getInstance().northeast(position) != null &&
            mapping.Map.getInstance().northeast(position).getNoise() > maxNoise)
        {
            maxNoise = mapping.Map.getInstance().northeast(position).getNoise();
            direction = 5;
        }

        return direction;
    }

    /**
     * Move o zumbi para uma nova lajota.
     */
    private void move()
    {
        int direction = findTarget();
        Tile destination = getDestination(direction);

        if (destination.getAlignment() == Global.HUMAN)
        {
            attack(destination);
            return;
        }

        if (destination.getBuilding() != null &&
            destination.getBuilding().getStatus() != Global.OVERRUN)
        {
            destination.getBuilding().beAttacked(meleeDamage);
            return;
        }

        position.removeUnit(this);

        destination.addUnit(this);

        position = destination;

        lastMove = mapping.Map.getInstance().getTurn();
    }

    /**
     * Faz um ataque contra uma lajota (construção e unidade).
     * @param targetTile a lajota a ser atacada
     */
    private void attack(Tile targetTile)
    {
        if(targetTile.getBuilding() != null) {
            targetTile.getBuilding().beAttacked(meleeDamage);
            view.addWarning(targetTile);
        }

        else if (targetTile.getUnitCount() > 0)
        {
            Unit targetUnit = targetTile.getUnit(0);
            super.meleeAttack(targetUnit);
            if (!targetUnit.wasAttackedByZombies()) {
                targetUnit.setAttackedByZombies(true);
                targetUnit.view.addWarning();
            }
            return;
        }

    }

    @Override
    public void startTurn()
    {
        if (lastMove >= mapping.Map.getInstance().getTurn())
            return;

        health += 10;
        if (health > maxHealth)
                health = maxHealth;

        move();
    }
}