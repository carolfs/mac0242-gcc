package units;

import buildings.Base;
import buildings.Building;
import global.Global;
import java.util.Random;
import mapping.Map;
import mapping.Tile;

/**
 * Soldado para matar os zumbis.
 * @see Unit
 */
public class Soldier extends Unit
{
    private buildings.Tower tower = null;
    private int ammoSpentInRangedAtack;
    
    public Soldier(Tile position)
    {
        health = maxHealth = 200;

        ranged = maxRanged = 2;
        rangedDamage = 30;
        ammoSpentInRangedAtack = rangedDamage / 10;
        range = 2;
        maxAmmo = ammo = 200;

        melee = maxMelee = 2;
        meleeDamage = 50;

        food = maxFood = 40;
        water = maxWater = 40;

        defence = 3;

        steps = maxSteps = 30;

        vision = 3;

        noise = 3;

        selected = false;

        alignment = Global.HUMAN;

        this.position = position;
        
        view = new gui.SoldierView(this);
    }
    
    /**
     * Indica se o soldado pode fazer um ataque a distância.
     * @return se o soldado pode fazer um ataque a distância
     */
    public boolean canDoRangedAttack() {
        if (!isInTower())
            return (getRanged() > 0 && ammo >= ammoSpentInRangedAtack);
        else
            return tower.canDoRangedAttack();
    }

    /**
     * Faz um ataque a distância contra uma unidade.
     * @param target a unidade a ser atacada
     * @return se o ataque foi bem sucedido
     */
    public boolean rangedAttack(Unit target)
    {
        if (target == null || getRanged() <= 0)
            return false;

        ammo -= ammoSpentInRangedAtack;
        ranged -= 1;

        double attacker = getHealth() * rangedDamage;
        double defender = target.getHealth() * target.defence;

        if (new Random().nextDouble() < attacker / (attacker + defender)) {
            target.health -= rangedDamage;
            if (!target.isAlive())
                target.die();
            return true;
        }
        else
            return false;
    }

    /**
     * @return the inTower
     */
    public boolean isInTower() {
        return (tower != null);
    }

    public void setTower(buildings.Tower tower) {
        this.tower = tower;
    }

    /**
     * @return the tower
     */
    public buildings.Tower getTower() {
        return tower;
    }
    
    public boolean canClearBuilding() {
        return (melee > 0);
    }

    public void clearBuilding()
    {
        if (!canClearBuilding())
                return;

        Building building = position.getBuilding();
        if (building != null &&
            building.getStatus() == Global.OVERRUN &&
            ammo >= building.getSize())
        {
            ammo -= building.getSize();
            melee -= 1;
            building.clear();
        }
    }
    
    @Override
    public boolean isAwaitingCommand() {
        return super.isAwaitingCommand() || canDoRangedAttack();
    }
    
    public boolean canMakeAmmo()
    {
        if (position.getBuilding() instanceof Base) {
            Base b = (Base) position.getBuilding();
            return b.canMakeAmmo();
        }
        else
            return false;
    }

    public void makeAmmo()
    {
        Base b = (Base) position.getBuilding();

        b.makeAmmo();
    }
}
