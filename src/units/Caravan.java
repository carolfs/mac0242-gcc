package units;

import global.Game;
import global.Global;
import java.util.ArrayList;
import java.util.Random;
import mapping.Tile;

/**
 * Caravana. Transporta unidades que chegam de outros locais para se unir
 * ao grupo de sobreviventes comandados pelo jogador.
 * @see Unit
 */
public class Caravan extends Unit
{
    ArrayList<Unit> units = new ArrayList<Unit>();
    int lastMove;
    int direction = -1;
    private boolean warned = false;

    public Caravan(Tile position)
    {
        noise = 4;
        vision = -1;

        this.position = position;
        lastMove = mapping.Map.getInstance().getTurn();
        alignment = Global.NEUTRAL;

        randomUnits();

        view = new gui.CaravanView(this);
        if (position.isSeen()) {
            view.addWarning();
            warned = true;
        }
    }

    public void addUnit(Unit unit)
    {
        if (unit.position != null)
            unit.position.removeUnit(unit);
        Game.getInstance().removeUnit(unit);
        units.add(unit);

        melee += unit.melee;
        maxMelee += unit.maxMelee;
        meleeDamage += unit.meleeDamage;
        defence += unit.defence;
        health += unit.health;
        maxHealth += unit.maxHealth;
        food += unit.food;
        water += unit.water;
    }

    private void randomUnits()
    {
        boolean stop = false;
        Random dice = new Random();

        for (int i = 0; i < 5; i++)
        {
            Unit unit;
            switch (dice.nextInt(6) % 6)
            {
                case 0:
                    unit = new Builder(null);
                    Game.getInstance().newUnit(unit);
                    addUnit(unit);
                    break;

                case 1:
                    unit = new Commoner(null);
                    Game.getInstance().newUnit(unit);
                    addUnit(unit);
                    break;
                    
                case 2:
                    unit = new Farmer(null);
                    Game.getInstance().newUnit(unit);
                    addUnit(unit);
                    break;

                case 3:
                    unit = new Scavenger(null);
                    Game.getInstance().newUnit(unit);
                    addUnit(unit);
                    break;
                    
                case 4:
                    unit = new Soldier(null);
                    Game.getInstance().newUnit(unit);
                    addUnit(unit);
                    break;

                case 5:
                default:
                    stop = true;
                    break;
            }
            if (units.isEmpty())
                stop = false;
            if (stop)
                break;
        }
    }

    public void dismantle()
    {
        Unit unit;

        if (isAlive() ||
            game.getHousing() < game.getPopulation() + units.size())
            return;


        while (!units.isEmpty())
        {
            unit = units.remove(0);
            unit.health = (int)(unit.maxHealth * (float)(health / maxHealth));

            melee -= unit.melee;
            maxMelee -= unit.maxMelee;
            meleeDamage -= unit.meleeDamage;
            defence -= unit.defence;
            health -= unit.health;
            maxHealth -= unit.maxHealth;
            food -= unit.food;
            water -= unit.water;


            position.addUnit(unit);
            Game.getInstance().newUnit(unit);
            unit.setPosition(position);
        }

        die();
    }

    public void turn()
    {
        while (!units.isEmpty())
        {
            Unit unit = units.remove(0);

            melee -= unit.melee;
            maxMelee -= unit.maxMelee;
            meleeDamage -= unit.meleeDamage;
            defence -= unit.defence;
            health -= unit.health;
            maxHealth -= unit.maxHealth;
            food -= unit.food;
            water -= unit.water;

            Zombie newZed = new Zombie(position);
            position.addUnit(newZed);
            Game.getInstance().newUnit(newZed);
        }

        die();
    }

    public Tile getDestination()
    {
        Tile destination = null;

        if (direction == -1)
            direction = new Random().nextInt(6);

        while (destination == null ||
               destination.getMovementCost() == Global.INFINITY)
        {
            switch(direction % 6)
            {
                case 0:
                    destination = mapping.Map.getInstance().east(position);
                    break;

                case 1:
                    destination = mapping.Map.getInstance().southeast(position);
                    break;

                case 2:
                    destination = mapping.Map.getInstance().southwest(position);
                    break;

                case 3:
                    destination = mapping.Map.getInstance().west(position);
                    break;

                case 4:
                    destination = mapping.Map.getInstance().northwest(position);
                    break;

                case 5:
                    destination = mapping.Map.getInstance().northeast(position);
                    break;
            }

            if (destination == null ||
                destination.getMovementCost() == Global.INFINITY)
                direction = new Random().nextInt(6);
        }
        return destination;
    }

    public boolean move()
    {
        Tile destination = getDestination();
        lastMove = mapping.Map.getInstance().getTurn();
        
        if (destination.getAlignment() == Global.ZOMBIE)
        {
            meleeAttack(destination.getUnit(0));
            return false;
        }

        this.goTo(destination, destination.getMovementCost());
        if (!warned && destination.isSeen()) {
            view.addWarning();
            warned = true;
        }
        return true;
    }

    @Override
    public void startTurn()
    {
        if (lastMove == mapping.Map.getInstance().getTurn())
            return;

        if (position.getAlignment() == Global.HUMAN)
        {
            dismantle();
            return;
        }

        if (food > 0)
        {
            food -= units.size();
            health += 5 * units.size();
        }
        else
            health -= units.size();

        if (water > 0)
        {
            water -= units.size();
            health += 5 * units.size();
        }
        else
            health -= units.size();

        if (health > maxHealth)
                health = maxHealth;

        melee = maxMelee;

        if (!isAlive())
        {
            turn();
            return;
        }

        int i = 0;
        while (i < 2)
        {
            if (move() || melee == 0)
                i++;
            
            if (position.getAlignment() == Global.HUMAN)
            {
                dismantle();
                return;
            }
        }
    }
}
