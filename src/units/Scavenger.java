package units;

import global.Global;
import java.util.Random;
import mapping.Tile;

/**
 * Explorador, pode pilhar estruturas e se esconder de zumbis.
 * @see Unit
 */
public class Scavenger extends Unit
{
    private boolean hidden = false;
    
    public Scavenger(Tile position)
    {
        health = maxHealth = 80;

        materials = 0;
        maxMaterials = 150;
        ammo = 0;
        maxAmmo = 500;
        food = 30;
        maxFood = 150;
        water = 30;
        maxWater = 150;


        melee = maxMelee = 5;
        meleeDamage = 20;

        defence = 1;

        steps = maxSteps = 40;

        vision = 4;

        noise = 2;

        selected = false;

        alignment = Global.HUMAN;

        this.position = position;
        
        view = new gui.ScavengerView(this);
    }

    /**
     * Esconde a unidade de forma que ela não faça "barulho" e portanto não atraia
     * zumbis.
     */
    public void hide()
    {
        if (!canHide())
            return;
        hidden = true;
        mapping.Map.getInstance().takeNoiseAndView(position, noise, vision);
        mapping.Map.getInstance().makeNoiseAndView(position, 0, vision);
    }

    public boolean canHide() {
        return hidden == false && steps == maxSteps && melee == maxMelee;
    }

    /**
     * Saqueia uma construção, recolhendo munição, comida, água e materiais.
     */
    public void pillage()
    {
        if (position.getBuilding() == null ||
            !position.getBuilding().hasLoot())
            return;

        int booty = new Random().nextInt(500);
        game.addAmmo(booty);
        booty = new Random().nextInt(120) + 30;
        game.addFood(booty);
        booty = new Random().nextInt(150);
        game.addMaterials(booty);
        booty = new Random().nextInt(120) + 30;
        game.addWater(booty);
        position.getBuilding().toggleLoot();
    }

    @Override
    public void replenish()
    {
        if (position.getBuilding() != null &&
            position.getAlignment() == Global.HUMAN)
        {
            if (game.addFood(food - (maxFood / 2)))
                food = (maxFood / 2);

            if (game.addWater(water - (maxWater / 2)))
                water = (maxWater / 2);
        }
    }

    @Override
    public void startTurn()
    {
        if (hidden)
        {
            mapping.Map.getInstance().takeNoiseAndView(position, 0, vision);
            mapping.Map.getInstance().makeNoiseAndView(position, noise, vision);
            hidden = false;
        }

        if (food > 0)
        {
            food -= 1;
            health += 5;
        }
        else
            health -= 1;

        if (water > 0)
        {
            water -= 1;
            health += 5;
        }
        else
            health -= 1;

        if (health > maxHealth)
                health = maxHealth;

        if (!isAlive())
            die();

        ranged = maxRanged;
        melee = maxMelee;
        steps = maxSteps;
        selected = false;

        replenish();
    }

    public boolean isHidden()
    {
        return hidden;
    }
    
    public boolean canPillage()
    {
        return hidden == false && position.getBuilding() != null &&
                position.getBuilding().hasLoot() &&
                position.getBuilding().getStatus() != global.Global.OVERRUN;
    }
    
    @Override
    public boolean canMove() {
        return super.canMove() && !hidden;
    }
    
    @Override
    public boolean canAttack() {
        return super.canAttack() && !hidden;
    }
}
