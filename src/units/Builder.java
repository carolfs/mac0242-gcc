package units;

import buildings.*;
import global.Game;
import global.Global;
import mapping.Tile;

/**
 * Um construtor.
 * @see Unit
 */
public class Builder extends Unit
{
    private static final int COST_DIRT_ROAD = 20;
    private static final int COST_WALL = 50;
    private boolean isBuilding = false;
    private boolean isUnbuilding = false;
    private int turnsLeft = -1;
    private Building building = null;

    private boolean isRepairing = false;

    public Builder(Tile position)
    {
        health = maxHealth = 120;

        maxMaterials = materials = 200;
        maxWater = water = 25;
        maxFood = food = 25;

        melee = maxMelee = 1;
        meleeDamage = 50;

        defence = 2;

        steps = maxSteps = 20;

        vision = 2;

        noise = 3;

        selected = false;

        alignment = Global.HUMAN;

        this.position = position;
        
        view = new gui.BuilderView(this);
    }

    /**
     * Cancela uma construção em andamento.
     */
    public void cancel()
    {
        if (isBuilding || isUnbuilding || isRepairing)
        {
            isRepairing = false;
            isBuilding = false;
            isUnbuilding = false;
            turnsLeft = -1;
            building = null;
        }
    }
    
    /**
     * Devolve se a unidade pode construir a estrutura ou construção pretendida.
     * @param option a opção de construção a ser utilizada
     * @return se a construção pode ocorrer
     */
    public boolean canBuild(char option)
    {
        switch(option)
        {
            case Global.BUILD_DIRT_ROAD:
                return materials >= COST_DIRT_ROAD;
            case Global.BUILD_WALL:
                return materials >= COST_WALL;
            case Global.BUILD_BASE:
                return materials >= Base.SIZE;
            case Global.BUILD_TOWER:
                return materials >= Tower.SIZE;
            case Global.BUILD_WAREHOUSE:
                return materials >= Warehouse.SIZE;
            case Global.BUILD_HOUSE:
                return materials >= House.SIZE;
            default:
                return false;
        }
    }

    /**
     * Constrói uma estrutura ou construção.
     * @param option uma opção de construção
     * @see Global
     */
    public void build(char option)
    {
        switch(option)
        {
            case Global.BUILD_DIRT_ROAD:
            if (!position.hasDirtRoad() &&
                !position.hasBridge())
            {
                position.addDirtRoad();
                materials -= COST_DIRT_ROAD;
            }
                break;

            case Global.BUILD_WALL:
            if (!position.hasWall() &&
                !position.hasBridge())
            {
                position.addWall();
                materials -= COST_WALL;
            }
                break;

            case Global.BUILD_BASE:
            if (position.getBuilding() == null)
                building = new Base(position);
                break;
            case Global.BUILD_TOWER:
            if (position.getBuilding() == null)
                building = new Tower(position);
                break;
            case Global.BUILD_WAREHOUSE:
            if (position.getBuilding() == null)
                building = new Warehouse(position);
                break;
            case Global.BUILD_HOUSE:
            if (position.getBuilding() == null)
                building = new House(position);
                break;
            default:
                return;
        }

        if (building != null &&
            building.getSize() <= materials)
        {
            turnsLeft = getBuilding().getSize() / 10;
            isBuilding = true;
        }
        else
            building = null;
    }
    
    /**
     * Destroi uma estrutura ou construção.
     * @param option uma opção de (des)construção
     * @see Global
     */
    public void unbuild(char option)
    {
        switch(option)
        {
            case Global.BUILD_DIRT_ROAD:
                if (position.hasDirtRoad())
                {
                    position.removeDirtRoad();
                    materials += COST_DIRT_ROAD * .75;
                }
                break;

            case Global.UNBUILD_ROAD:
                if (position.hasRoad())
                {
                    position.removeRoad();
                    materials += 30;
                }
                break;

            case Global.BUILD_WALL:
                if (position.hasWall())
                {
                    position.removeWall();
                    materials += COST_WALL * .75;
                }
                break;

            case Global.UNBUILD_FOREST:
                if (position.hasForest())
                {
                    position.removeForest();
                    materials += 15;
                }
                break;

            case Global.BUILD_FERTILE_LAND:
                if (position.isFertile())
                {
                    position.removeFertile();
                }
                break;


            case Global.BUILD_BUILDING:
                if (position.getBuilding() == null)
                    return;
                building = position.getBuilding();
                turnsLeft = getBuilding().getSize() / 10;
                isUnbuilding = true;
                break;
            default:
                return;
        }
    }

    private void finishBuilding()
    {
        Game.getInstance().newBuilding(getBuilding());
        position.setBuilding(getBuilding());
        materials -= getBuilding().getSize();
        isBuilding = false;
        building = null;
        turnsLeft = -1;
    }

    private void finishUnbuilding()
    {
        Game.getInstance().removeBuilding(getBuilding());
        position.setBuilding(null);
        materials += (int) (-.75 * getBuilding().getSize());
        isUnbuilding = false;
        building = null;
        turnsLeft = -1;
    }

    /**
     * Repara uma construção que tinha sido infestada por zumbis.
     */
    public void repair()
    {
        building = position.getBuilding();
        
        if (building != null &&
            building.getStatus() == Global.AWAITING_REPAIR ||
            building.isDamaged())
            isRepairing = true;
    }

    private void finishRepair()
    {
        if (building != null)
            building.restore();

        building = null;
        isRepairing = false;
    }

    @Override
    public void startTurn()
    {
        if (food > 0)
        {
            food -= 1;
            health += 5;
        }
        else
            health -= 1;

        if (water > 0)
        {
            water -= 1;
            health += 5;
        }
        else
            health -= 1;

        if (health > maxHealth)
                health = maxHealth;

        if (!isAlive())
            die();

        if (isRepairing)
            finishRepair();

        if (isBuilding)
        {
            turnsLeft -= 1;
            if (getTurnsLeft() == 0)
                finishBuilding();
            else
                return;
        }

        if (isUnbuilding)
        {
            turnsLeft -= 1;
            if (getTurnsLeft() == 0)
                finishUnbuilding();
            else
                return;
        }

        ranged = maxRanged;
        melee = maxMelee;
        steps = maxSteps;
        selected = false;

        replenish();
    }

    /**
     * @return the isBuilding
     */
    public boolean getIsBuilding() {
        return isBuilding;
    }

    /**
     * @return the isUnbuilding
     */
    public boolean getIsUnbuilding() {
        return isUnbuilding;
    }

    /**
     * @return the turnsLeft
     */
    public int getTurnsLeft() {
        return turnsLeft;
    }

    /**
     * @return the building
     */
    public Building getBuilding() {
        return building;
    }
    
    public boolean getIsRepairing()
    {
        return isRepairing;
    }
    
    public boolean isBusy()
    {
        return getIsBuilding() || getIsUnbuilding() || getIsRepairing();
    }
}
