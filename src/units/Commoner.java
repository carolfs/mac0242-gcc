package units;

import buildings.Base;
import global.Global;
import mapping.Tile;

/**
 * Leigos. Podem ser treinados em uma base militar para adquirir uma profissão
 * (virarem outros tipos de unidade).
 * @see Unit
 */
public class Commoner extends Unit
{
    private int trainingIndex = -1;
    
    public Commoner(Tile position)
    {
        health = maxHealth = 80;

        food = maxFood = 20;
        water = maxWater = 20;

        materials = maxMaterials = 0;
        ammo = maxAmmo = 0;

        melee = maxMelee = 1;
        meleeDamage = 20;

        defence = 1;

        steps = maxSteps = 20;

        vision = 2;

        noise = 3;

        selected = false;

        alignment = Global.HUMAN;

        this.position = position;
        
        view = new gui.CommonerView(this);
    }

    /**
     * @return the training
     */
    public boolean isTraining() {
        return getTrainingIndex() >= 0;
    }

    public void setTrainingIndex(int trainingIndex) {
        this.trainingIndex = trainingIndex;
    }

    /**
     * @return the trainingIndex
     */
    public int getTrainingIndex() {
        return trainingIndex;
    }

    public void cancel()
    {
        if (!isTraining())
            return;
        ((Base) position.getBuilding()).cancel(trainingIndex);
        trainingIndex = -1;
    }
    
    @Override
    public boolean isAwaitingCommand() {
        return super.isAwaitingCommand() && !isTraining();
    }
}
